  function show_upload(param){
    // Preview
    $("#input").val(param);
    $("#modal_upload").modal('show');
  }

  function previewFile() {
    const att = $("#input").val();
    const preview = $("#"+att+"_preview");
    const base64 = $("#"+att);
    const file = document.querySelector('input[type=file]').files[0];
    const reader = new FileReader();

    reader.addEventListener("load", function () {
      base64.val(reader.result);
      $("#preview_img").html('<img src="'+reader.result+'" alt="" class="img-fluid img-thumbnail animated fadeIn mb-2">');
      // $("#preview_img").attr('style','background-image: url(\''+reader.result+'\');');
    }, false);

    if (file) {
      reader.readAsDataURL(file);
    }
  }


  function upload(){
      const att = $("#input").val();
      const preview = $("#"+att+"_preview");
      const base64 = $("#"+att).val();
     // preview.html('<img src="'+base64+'" alt="" class="img-fluid img-thumbnail animated fadeIn mt-2">');
     preview.attr('style','background-image: url(\''+base64+'\');');

     $("#modal_upload").modal('hide');
     $("#input").val('');
       $("#preview_img").html('');
  }

  function close(){
     $("#modal_upload").modal('hide');
     $("#input").val('');
       $("#preview_img").html('');
  }
 