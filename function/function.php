<?php 
require_once("../config/config.php");
require_once("../config/auth.php");
require_once("../core/validator.php");
require_once("../core/helper.php");

 
$db = new db();

$action = $_REQUEST['action'];

$auth_id = (Auth::check())?  Auth::user('user_id') : '';


function get_vaccines_data($child_id, $app_id, $db){
	$array = array('child_id' => $child_id, 'appointment_id' => $app_id);
	$query = "SELECT a.vaccine_name from tbl_vaccine a 
	INNER JOIN tbl_appointment_detail b on a.vac_id = b.vac_id
	where a.deleted_at is null and b.child_id = :child_id and b.appointment_id=:appointment_id";
	$res = $db->get($array,$query);

	$fetch_arr = array();
	foreach ($res as $data) {
		$fetch_arr[] = $data->vaccine_name;
	}

	return (!empty($fetch_arr))? implode(',', $fetch_arr) : 'NO VACCINES FOUND!';
}

switch ($action) {
	case 'login':
		$v = new Validator();

		 $v->make($_POST, [
		 	'username' => 'required',
		 	'password' => 'required'
		 ]);

		 $data = array('username' => $_POST['username'], 'password' => $_POST['password']);

		 if (!$v->fail) {
			 	echo $db->json(['status' => false, 'error' => $v->message]);
		 }else{
			 	echo $db->login('tbl_users', $data);
		 }
	break;

	case 'forgot_password':
		$v = new Validator();

		 $v->make($_POST, [
		 	'username' => 'required',
		 ]);

		 if (!$v->fail) {
			 	echo $db->json(['status' => false, 'error' => $v->message]);
		 }else{
		 	$array = array('username' => $_POST['username']);
		 	$sql = "SELECT * from tbl_users where username=:username limit 1";
		 	$res = $db->find($array, $sql);

		 	if ($res !== false) {
		 		$res->security_answer1 = md5($res->security_answer1);
			 	$res->security_answer2 = md5($res->security_answer2);
		 	}

 		    echo $db->json(['status' => true, 'data' => $res]);
		 }
	break;

	case 'check_answer':
		$v = new Validator();

		 $v->make($_POST, [
		 	'security_question' => 'required',
		 	'security_answer' => 'required'
		 ]);

		 if (!$v->fail) {
			 	echo $db->json(['status' => false, 'error' => $v->message]);
		 }else{
		 	if ($_POST['security_question'] == md5($_POST['security_answer'])) {
			 	echo $db->json(['status' => true, 'message' => 'Password match!']);
		 	}else{
			 	echo $db->json(['pass' => false, 'message' => 'Password not match!']);
		 	}
		 }
	break;

	case 'recover_child':
		$data = array('child_id' => $_POST['id'],
	 					'deleted_at' => null);

 		if ($db->update('tbl_child', $data)) {
		 	echo $db->json(['status' => true, 'message' => 'Child record successfully recovered!']);
		 	$db->log('Recover Child with the ID of'. $_POST['id'], 'recover child', $_POST['id']);
 		}
	break;

	case 'recover_user':
		$data = array('user_id' => $_POST['id'],
	 					'deleted_at' => null);
 		if ($db->update('tbl_users', $data)) {
		 	echo $db->json(['status' => true, 'message' => 'Account successfully recovered!']);
		 	$db->log('Recover Account with the ID of'. $_POST['id'], 'recover account', $_POST['id']);
 		}
	break;

	case 'change_pass':
		$v = new Validator();

		 $v->make($_POST, [
		 	'new_password' => 'required',
		 	'confirm_password' => 'required'
		 ]);

		 if (!$v->fail) {
			 	echo $db->json(['status' => false, 'error' => $v->message]);
		 }else{
		 	if ($_POST['new_password'] == $_POST['confirm_password']) {
		 		$data = array('user_id' => $_POST['user_id'],
	 					'password' => password_hash($_POST['confirm_password'], PASSWORD_DEFAULT));
		 		if ($db->update('tbl_users', $data)) {
				 	echo $db->json(['status' => true, 'message' => 'Password changed successfully!']);
				 	$db->log('Change Password', 'Changed Password', $_POST['user_id']);
		 		}

		 	}else{
			 	echo $db->json(['pass' => false, 'message' => 'Password doesn\'t match!']);
		 	}
		 }
	break;

	case 'get_stats_data':
		$data = array();
		$array = array();
		$param = $_GET['param'];

		$title = ($param == 'all')? 'Overall' : 'Purok '.$param;
		
		$add_where_count = ($param == 'all')? '' : "and purok = ".$param.'';

		$total_child_live = $db->counter('tbl_child','child_id',"where deleted_at is null and (is_death is null or is_death = 0) and archive_at is null ".$add_where_count."");

		$total_child = $db->counter('tbl_child','child_id',"where deleted_at is null ".$add_where_count."");
		$total_death_child = $db->counter('tbl_child','child_id',"where deleted_at is null and is_death = 1 ".$add_where_count."");
		$total_vaccine = $db->sum('tbl_vaccine','doses',"where deleted_at is null");
		
		$dash_titles = $title.' Children\'s Statistical Information';
 		
 		$add_where = ($param == 'all')? '' : "and a.purok = ".$param.'';

		// Get total vaccine immune
		$query = "SELECT b.child_id as child_id, count(b.vac_id) as vaccine_count from tbl_child a
		LEFT JOIN tbl_appointment_detail b on a.child_id = b.child_id 
		where a.deleted_at is null and (a.is_death is null or a.is_death = 0) and b.is_finished is not null ".$add_where." GROUP BY b.child_id";
		$res = $db->get($array,$query);
		$total_immune = 0;
		$total_not_immune = 0;

		$immunized = array();
		$on_going = array();
		$dead = array();


		$q = "SELECT * from tbl_child where deleted_at is null and (is_death is null or is_death = 0)";
		$qres = $db->get($array, $q);

		foreach ($qres as $row) {
			$arrays = array('child_id' => $row->child_id); 
			$querys = "SELECT b.child_id as child_id, count(b.vac_id) as vaccine_count from tbl_child a
			LEFT JOIN tbl_appointment_detail b on a.child_id = b.child_id 
			where b.child_id = :child_id and a.deleted_at is null and (a.is_death is null or a.is_death = 0) and b.is_finished is not null ".$add_where." GROUP BY b.child_id";
			$ress = $db->get($arrays,$querys);

			foreach ($ress as $vals) {
				if ($vals->vaccine_count >= $total_vaccine) {
					$immunized[] = $row;
				}else if ($vals->vaccine_count < $total_vaccine) {
					$on_going[] = $row;
				}
			}

		}


		foreach ($res as $val) {
			if ($val->vaccine_count >= $total_vaccine) {
				$total_immune++;
			}else if ($val->vaccine_count < $total_vaccine) {
				$total_not_immune++;
			}
		}

		$immuned = get_percentage($total_child, $total_immune);
		$not_immune = get_percentage($total_child, $total_not_immune);
		$total_death = get_percentage($total_child, $total_death_child);
		$total_child_ = get_percentage($total_child, $total_child);
		$total_child_alive = get_percentage($total_child, $total_child_live);

		$not_immune_remedy_total = ($total_not_immune == 0)? $total_child_alive : $not_immune;


			$arrayss = array();
			$queryss = "SELECT a.*,b.* from tbl_death_record a
			INNER JOIN tbl_child b on a.child_id=b.child_id order by a.death_id desc";
			$resss = $db->get($arrayss,$queryss);

		$data[] = array("name" => 'Immunized', "y" => $immuned, "drilldown" => 'Immunized', "color" => '#28A745', 'datas' => $immunized);
		$data[] = array("name" => 'On Going', "y" => $not_immune_remedy_total, "drilldown" => 'On Going', "color" => '#85c493', 'datas' => $on_going);
		$data[] = array("name" => 'Deceased', "y" => $total_death, "drilldown" => 'Deceased', "color" => '#FF5D5D', 'datas' => $resss);

		echo $db->json(['status' => true, 'data' => json_encode($data), 'date' => $dash_titles.'. '.date('F d, Y')]);
		# code...
	break;

	case 'finish_all_check':
		$selected_child = $_POST['selected_child'];
		$date = date('Y-m-d H:i:s');

		$i = 0;
		foreach ($selected_child as $ids) {
			$data = array('appointment_id' => $ids, 'is_finished' => $date);
	 		if ($db->update('tbl_appointment', $data)) {
			 	$db->log('Marked Schedule as finished with the schedule ID of '.$ids, 'Appointment', $auth_id);
	 			$i++;
	 		}
		}
		
		$message = $i.' Schedule marked as finished successfully!';
		if ($i > 0) {
			echo $db->json(['status' => true, 'message' => $message]);
		}else{
			echo $db->json(['status' => false, 'message' => 'No Item selected!']);	
		}
	break;

	case 'delete_all_check':
		$selected_child = $_POST['selected_child'];
		$i = 0;
		foreach ($selected_child as $ids) {
			$array = array('appointment_id' => $ids);
			if ($db->softDelete('tbl_appointment', $array)) {
			 	$db->log('Marked Schedule as delete with the schedule ID of '.$ids, 'Appointment', $auth_id);
				$i++;
			}
		}
		
		$message = $i.' Schedule deleted successfully!';
		if ($i > 0) {
			echo $db->json(['status' => true, 'message' => $message]);	
		}else{
			echo $db->json(['status' => false, 'message' => 'No Item selected!']);	
		}
	break;

	case 'show_child_milestone':
		$array = array('child_id' => $_POST['child_id']);
		$query = "SELECT * from tbl_appointment where child_id = :child_id";
		$res = $db->get($array,$query);
		echo $db->json(['status' => true, 'data' => $res]);
	break;

	case 'find_user':
		$id = $_GET['id'];
		$array = array('id' => $id);
		$query = "SELECT * from tbl_users where user_id = :id order by user_id desc";
		$res = $db->find($array,$query);
		echo $db->json(['status' => true, 'data' => $res]);
	break;

	case 'show_child_death':
		$array = array();
		$query = "SELECT a.*,b.* from tbl_death_record a
		INNER JOIN tbl_child b on a.child_id=b.child_id order by a.death_id desc";
		$res = $db->get($array,$query);
		echo $db->json(['status' => true, 'data' => $res]);
	break;


	case 'delete_midwife':
		if ($db->softDelete('tbl_users', $_POST['id'], 'user_id')) {
			echo $db->json(['status' => true, 'message' => 'Midwife successfully deleted!']);
		}
	break;

	case 'delete_healt_worker':
		if ($db->softDelete('tbl_users', $_POST['id'], 'user_id')) {
			echo $db->json(['status' => true, 'message' => 'Health Worker successfully deleted!']);
		}
	break;

	case 'delete_child_death':
		$array = array('death_id' => $_POST['id']);
		
		$data = array('child_id' => $_POST['child'],
					  'is_death' => 0);

		// update child not death
		$db->update('tbl_child', $data);
		if ($db->delete('tbl_death_record', $array)) {
			echo $db->json(['status' => true, 'message' => 'Deceased record successfully deleted!']);
		}
	break;

	case 'delete_schedule':
		$array = array('appointment_id' => $_POST['id']);
		if ($db->delete('tbl_appointment', $array)) {
			echo $db->json(['status' => true, 'message' => 'Schedule successfully deleted!']);
		}
	break;

	case 'mark_finish_schedule':
		$date = date('Y-m-d H:i:s');
		$data = array('appointment_id' => $_POST['id'], 'is_finished' => $date);
 		if ($db->update('tbl_appointment', $data)) {
 			echo $db->json(['status' => true, 'message' => 'Record has updated successfully!']);
 		}
	break;

	case 'get_schedule':
		$status = 'On Time';
		$new_data = array();
		$array = array();
		$where = '';
		$date_from = $_GET['date_from'];
		$date_to = $_GET['date_to'];

		if (!empty($date_from) && !empty($date_to)) {
			$array = array('date_from' => $date_from, 'date_to' => $date_to);

			$where = "WHERE DATE(a.appointment_date) BETWEEN DATE(:date_from) AND DATE(:date_to)";
		}elseif (empty($date_to) && empty($date_from)) {
			$array = array();
			$where = '';
		}

		$query = "SELECT DATE(NOW()) as today,a.*,b.*,a.description,a.appointment_id,a.is_finished as status,b.gender, b.contact_number,DATE(a.appointment_date) as date_appoinment, concat(b.lastname,' ', b.firstname) as fullname,b.middlename from tbl_appointment a
			INNER JOIN tbl_child b on a.child_id=b.child_id ".$where;

		$res = $db->get($array,$query);

		foreach ($res as $row) {
			if(empty($row->status)){
				$row->status_msg = '<span class="badge badge-danger p-2">No Appearance</span>';
			}elseif (strtotime(date('Y-m-d', strtotime($row->status))) > strtotime(date('Y-m-d', strtotime($row->date_appoinment)))) {
				$row->status_msg = '<span class="badge badge-warning p-2">Late</span>';
			}else{
				$row->status_msg = '<span class="badge badge-success p-2">'.$status.'</span>';
			}

			$row->dt_app = date('M d, Y (l)', strtotime($row->date_appoinment));
			$row->dt_finished = date('M d, Y (l)', strtotime($row->status));

			$date = date('Y');
			$bdate = date('Y', strtotime($row->birthdate));

			$mdate = date('m');
			$mbdate = date('m', strtotime($row->birthdate));

			$m = abs($mdate - $mbdate);
			$y = abs($date - $bdate);

			$year = ($y < 0)? 0 : $y;
			$month = ($year > 0)? '' : ($m > 0)? '.'.$m : '';

			$prefix = ($year == 0)? ' Month(s) Old' : ' Year(s) Old';

			$age = $year.$month.$prefix;

			$row->vacines_detail = get_vaccines_data($row->child_id, $row->appointment_id, $db);

			$row->child_age = $age;

			$new_data[] = $row;
		}

		echo $db->json(['status' => true, 'data' => $res]);
	break;

	case 'show_all_on_sched':
		$date_sched = $_GET['date_sched'];
		$status = 'On Time';
		// echo $date_sched;
		$new_data = array();
		$array = array('date_sched' => $date_sched);
		$query = "SELECT a.*,b.*,DATE(NOW()) as today,a.description,a.appointment_id,a.is_finished as status,b.gender, b.contact_number,DATE(a.appointment_date) as date_appoinment, concat(b.lastname,' ', b.firstname) as fullname,b.middlename from tbl_appointment a
				 	INNER JOIN tbl_child b on a.child_id=b.child_id 
				 	where DATE(a.appointment_date) = DATE(:date_sched)";
		$res = $db->get($array,$query);

		foreach ($res as $row) {
			if(empty($row->status)){
				$row->status_msg = '<span class="badge badge-danger p-2">No Appearance</span>';
			}elseif (strtotime(date('Y-m-d', strtotime($row->status))) > strtotime(date('Y-m-d', strtotime($row->date_appoinment)))) {
				$row->status_msg = '<span class="badge badge-warning p-2">Late</span>';
			}else{
				$row->status_msg = '<span class="badge badge-success p-2">'.$status.'</span>';
			}

			$date = date('Y');
			$bdate = date('Y', strtotime($row->birthdate));

			$mdate = date('m');
			$mbdate = date('m', strtotime($row->birthdate));

			$m = abs($mdate - $mbdate);
			$y = abs($date - $bdate);

			$year = ($y < 0)? 0 : $y;
			$month = ($year > 0)? '' : ($m > 0)? '.'.$m : '';

			$prefix = ($year == 0)? ' Month(s) Old' : ' Year(s) Old';

			$age = $year.$month.$prefix;

			$row->vacines_detail = get_vaccines_data($row->child_id, $row->appointment_id, $db);

			$row->child_age = $age;

			$new_data[] = $row;
		}

		$title = date('F d, Y D', strtotime($date_sched));
		echo $db->json(['status' => true, 'data' => $res, 'title' => $title]);
		# code...
	break;

	case 'vaccines_list':
		$array = array();
		$query = "SELECT * from tbl_vaccine where deleted_at is null";
		$res = $db->get($array,$query);
		echo $db->json(['status' => true, 'data' => $res]);
	break;

	case 'log_list':
		$array = array();
		$query = "SELECT a.*,b.*, concat(b.firstname,' ',b.lastname) as logger, a.created_at as date_action from tbl_logs a 
		INNER JOIN tbl_users b on a.user_id=b.user_id
		order by a.log_id desc";
		$res = $db->get($array,$query);
		echo $db->json(['status' => true, 'data' => $res]);
	break;

	case 'add_vaccine':
		 $v = new Validator();
		 $v->make($_POST, [
		 	'vaccine_name' => 'required',
		 ]);

		 if (!$v->fail) {
			 echo $db->json(['status' => false, 'error' => $v->message]);
		 }else{
		 	if (!empty($_POST['vac_id'])) {
		 	  $data = array("vac_id" => $_POST['vac_id'],
							"vaccine_name" => $_POST['vaccine_name'],
							'doses' => $_POST['doses'],
							'minimum_age' => $_POST['minimum_age'],
							'description' => $_POST['description'],
							'updated_at' => date('Y-m-d H:i:s A'),
							'created_by' => $auth_id);

		 		if ($db->update('tbl_vaccine', $data)) {
		 			echo $db->json(['status' => true, 'message' => 'Vaccine updated successfully!']);
		 			$db->log('updated vaccine name as '.$_POST['vaccine_name'].' with the vaccine ID '.$_POST['vac_id'], 'Vaccine', $auth_id);
		 		}
		 	}else{
		 		 $data = array("vaccine_name" => $_POST['vaccine_name'],
							'doses' => $_POST['doses'],
							'minimum_age' => $_POST['minimum_age'],
							'description' => $_POST['description'],
							'created_at' => date('Y-m-d H:i:s A'),
							'created_by' => $auth_id);

		 		if ($db->save('tbl_vaccine', $data)) {
		 			echo $db->json(['status' => true, 'message' => 'Vaccine saved successfully!']);
		 			$db->log('added vaccines with the vaccine name '.$_POST['vaccine_name'], 'Vaccine', $auth_id);

		 		}
		 	}
		 }
	break;

	case 'delete_vaccines':
		if ($db->softDelete('tbl_vaccine', $_POST['vac_id'], 'vac_id')) {
			echo $db->json(['status' => true, 'message' => 'Vaccine successfully deleted!']);
		 	$db->log('deleted vaccine with the vaccine ID '.$_POST['vac_id'], 'Vaccine', $auth_id);

		}
	break;


	case 'health_worker_list':
		$array = array();
		$query = "SELECT * from tbl_users where user_type = 3 and deleted_at is null order by user_id desc";
		$res = $db->get($array,$query);
		echo $db->json(['status' => true, 'data' => $res]);
	break;

	case 'health_worker_list_archive':
		$array = array();
		$query = "SELECT * from tbl_users where user_type = 3 and deleted_at is not null order by user_id desc";
		$res = $db->get($array,$query);
		echo $db->json(['status' => true, 'data' => $res]);
	break;

	case 'midwife_list':
		$array = array();
		$query = "SELECT * from tbl_users where user_type = 2 and deleted_at is null order by user_id desc";
		$res = $db->get($array,$query);
		echo $db->json(['status' => true, 'data' => $res]);
	break;

	case 'midwife_list_archive':
		$array = array();
		$query = "SELECT * from tbl_users where user_type = 2 and deleted_at is not null order by user_id desc";
		$res = $db->get($array,$query);
		echo $db->json(['status' => true, 'data' => $res]);
	break;

	case 'add_user':
		 $user_what = ($_POST['user_type'] == 2) ? 'Midwife' : 'Health Worker';

		 $v = new Validator();

		 $v->make($_POST, [
		 	'firstname' => 'required',
		 	'lastname' => 'required',
			'birthdate' => 'required',
			'gender' => 'required',
			'address' => 'required',
			'username' => 'required',
			'password' => 'required',
			'security_question1' => 'required',
			'security_answer1' => 'required',
			'security_question2' => 'required',
			'security_answer2' => 'required'
		 ]);

		 if (!$v->fail) {
			 echo $db->json(['status' => false, 'error' => $v->message]);
		 }else{
		 	if (!empty($_POST['user_id'])) {
		 	  $data = array("user_id" => $_POST['user_id'],
							"firstname" => $_POST['firstname'],
							"lastname" => $_POST['lastname'],
							"birthdate" => $_POST['birthdate'],
							"gender" => $_POST['gender'],
							"address" => $_POST['address'],
							"username" => $_POST['username'],
							"user_type" => $_POST['user_type'],
							'security_question1' => $_POST['security_question1'],
							'security_answer1' => $_POST['security_answer1'],
							'security_question2' => $_POST['security_question2'],
							'security_answer2' => $_POST['security_answer2'],
							'updated_at' => date('Y-m-d H:i:s A'),
							'created_by' => $auth_id);

		 		if ($db->update('tbl_users', $data)) {

		 			echo $db->json(['status' => true, 'message' => $user_what.' account updated successfully!']);
		 			$db->log('updated user account '.$_POST['lastname'].' '.$_POST['firstname'], 'User Account', $auth_id);

		 		}
		 	}else{
		 		$data = array("firstname" => $_POST['firstname'],
							"lastname" => $_POST['lastname'],
							"birthdate" => $_POST['birthdate'],
							"gender" => $_POST['gender'],
							"address" => $_POST['address'],
							"username" => $_POST['username'],
							"password" => password_hash($_POST['password'], PASSWORD_DEFAULT),
							"user_type" => $_POST['user_type'],
							'security_question1' => $_POST['security_question1'],
							'security_answer1' => $_POST['security_answer1'],
							'security_question2' => $_POST['security_question2'],
							'security_answer2' => $_POST['security_answer2'],
							'created_at' => date('Y-m-d H:i:s A'),
							'created_by' => $auth_id);

		 		if ($db->save('tbl_users', $data)) {
		 			echo $db->json(['status' => true, 'message' => $user_what.' account saved successfully!']);
		 			$db->log('added user account '.$_POST['lastname'].' '.$_POST['firstname'], 'User Account', $auth_id);
		 		}
		 	}
		 }
	break;

	case 'update_user':
		 $user_what = ($_POST['user_type'] == 1)? 'Admin' : ($_POST['user_type'] == 2) ? 'Midwife' : 'Health Worker';

		 $v = new Validator();
		 
		 $v->make($_POST, [
		 	'firstname' => 'required',
		 	'lastname' => 'required',
			'birthdate' => 'required',
			'gender' => 'required',
			'address' => 'required',
			'username' => 'required',
		 ]);

		 if (!$v->fail) {
			 echo $db->json(['status' => false, 'error' => $v->message]);
		 }else{
		 	if (!empty($_POST['user_id'])) {
		 	  $data = array("user_id" => $_POST['user_id'],
							"avatar" => $_POST['avatar'],
							"firstname" => $_POST['firstname'],
							"lastname" => $_POST['lastname'],
							"birthdate" => $_POST['birthdate'],
							"gender" => $_POST['gender'],
							"address" => $_POST['address'],
							"username" => $_POST['username'],
							"user_type" => $_POST['user_type'],
							'updated_at' => date('Y-m-d H:i:s A'));

		 		if ($db->update('tbl_users', $data)) {
		 			$array = array('id' => $_POST['user_id']);
					$query = "SELECT * from tbl_users where user_id = :id order by user_id desc";
					$res = $db->find($array,$query);
		 			$db->store_auth($res);
		 			echo $db->json(['status' => true, 'message' => $user_what.' account updated successfully!']);
		 			$db->log('updated user account '.$_POST['lastname'].' '.$_POST['firstname'], 'User Account', $auth_id);

		 		}
		 	}
		 }
	break;


	case 'get_appointment_details':
		$id = $_GET['id'];
		$array = array('appointment_id' => $id);
		$query = "SELECT a.*,b.* from tbl_appointment_detail 
		a INNER JOIN tbl_vaccine b on a.vac_id=b.vac_id
		where a.appointment_id=:appointment_id";
		
		$res = $db->get($array,$query);
		echo $db->json(['status' => true, 'data' => $res]);
	break;

	case 'get_appointment_child':
		$id = $_GET['id'];

		$array = array('appointment_id' => $id);
		$query = "SELECT a.*,b.* from tbl_appointment 
		a INNER JOIN tbl_child b on a.child_id=b.child_id
		where a.appointment_id=:appointment_id";
		
		$res = $db->find($array,$query);
		$date = date('Y');
		$bdate = date('Y', strtotime($res->birthdate));

		$mdate = date('m');
		$mbdate = date('m', strtotime($res->birthdate));

		$m = abs($mdate - $mbdate);
		$y = abs($date - $bdate);

		$year = ($y < 0)? 0 : $y;
		$month = ($year > 0)? '' : ($m > 0)? '.'.$m : '';

		$prefix = ($year == 0)? ' Month(s) Old' : ' Year(s) Old';

		$age = $year.$month.$prefix;

		$appointment_date = date('M d, Y', strtotime($res->appointment_date));

		echo $db->json(['status' => true, 'data' => $res, 'age' => $age, 'appointment_date' => $appointment_date]);

	break;
	
	case 'children_list':
		$array = array();
		$query = "SELECT * from tbl_child where deleted_at is null and (is_death is null or is_death = 0) order by child_id desc";
		$res = $db->get($array,$query);
		echo $db->json(['status' => true, 'data' => $res]);
	break;

	case 'children_list_archive':
		$array = array();
		$query = "SELECT * from tbl_child where deleted_at is not null or is_death = 1 order by child_id desc";
		$res = $db->get($array,$query);
		echo $db->json(['status' => true, 'data' => $res]);
	break;

	case 'children_list_archive_graduate':
		$array = array();
		$query = "SELECT * from tbl_child where archive_at is not null order by child_id desc";
		$res = $db->get($array,$query);
		echo $db->json(['status' => true, 'data' => $res]);
	break;


	case 'add_child':
		$v = new Validator();

		 $v->make($_POST, [
		 	'firstname' => 'required',
			'lastname' => 'required',
			'birthdate' => 'required',
			'gender' => 'required',
			'purok' => 'required',
			'street' => 'required',
			'kind_of_birth' => 'required',
			'mother_name' => 'required',
			'father_name' => 'required',
			'contact_number' => 'required'
		 ]);


		 if (!$v->fail) {
			 	echo $db->json(['status' => false, 'error' => $v->message]);
		 }else{
		 	if (!empty($_POST['child_id'])) {
		 		$data = array(
		 		'child_id' => $_POST['child_id'],
		 		'firstname' => $_POST['firstname'],
				'middlename' => $_POST['middlename'],
				'lastname' => $_POST['lastname'],
				'birthdate' => $_POST['birthdate'],
				'gender' => $_POST['gender'],
				'purok' => $_POST['purok'],
				'street' => $_POST['street'],
				'kind_of_birth' => $_POST['kind_of_birth'],
				'mother_name' => $_POST['mother_name'],
				'father_name' => $_POST['father_name'],
				'contact_number' => $_POST['contact_number']);
		 		if ($db->update('tbl_child', $data)) {
		 			echo $db->json(['status' => true, 'message' => 'Children profile updated successfully!']);
		 			$db->log('updated child profile '.$_POST['lastname'].' '.$_POST['firstname'], 'Child', $auth_id);

		 		}
		 	}else{
 		 		$data = array('firstname' => $_POST['firstname'],
				'middlename' => $_POST['middlename'],
				'lastname' => $_POST['lastname'],
				'birthdate' => $_POST['birthdate'],
				'gender' => $_POST['gender'],
				'purok' => $_POST['purok'],
				'street' => $_POST['street'],
				'kind_of_birth' => $_POST['kind_of_birth'],
				'mother_name' => $_POST['mother_name'],
				'father_name' => $_POST['father_name'],
				'contact_number' => $_POST['contact_number']);
		 		
		 		if ($db->save('tbl_child', $data)) {
		 			echo $db->json(['status' => true, 'message' => 'Children profile saved successfully!']);
		 			$db->log('added child '.$_POST['lastname'].' '.$_POST['firstname'], 'Child', $auth_id);

		 		}
		 	}
		 }
	break;

	case 'list_sched':
		$array = array();
		$query = "SELECT concat('#00c0ef') as borderColor,concat('#00c0ef') as backgroundColor, concat(count(appointment_id),' ','Scheduled') as title, DATE(appointment_date) as start,DATE(appointment_date) as id from tbl_appointment group by DATE(appointment_date)";
		$res = $db->get($array,$query);
		echo $db->json(['status' => true, 'data' => $res]);
	break;

	case 'delete_child':
		if ($_POST['type_delete'] == '1') {
			$data = array('child_id' => $_POST['child_id']);

			if ($db->delete('tbl_child', $data)) {
				echo $db->json(['status' => true, 'message' => 'Child permanently deleted!']);
			}
		}elseif($_POST['type_delete'] == '2'){
			if ($db->softDelete('tbl_child', $_POST['child_id'], 'child_id')) {
				echo $db->json(['status' => true, 'message' => 'Child archived successfully!']);
			}
		}else{
			$data = array('child_id' => $_POST['child_id'], 'archive_at' => date('Y-m-d H:i:s A'));
			
			if ($db->update('tbl_child', $data)) {
				echo $db->json(['status' => true, 'message' => 'Child marked as graduated!']);
			}
		}
		
	break;

	case 'delete_appointment_detail':
		$data = array('detail_id' => $_POST['detail_id']);

		if ($db->delete('tbl_appointment_detail', $data)) {
			echo $db->json(['status' => true, 'message' => 'Vaccine successfully deleted!']);
		}
	break;

	case 'save_sched':
		$v = new Validator();

		 $v->make($_POST, [
		 	'child_id' => 'required',
		 	'appointment_date' => 'required',
		 	'appointment_time' => 'required',
		 	'vac_id' => 'required'
		 ]);

		 if (!$v->fail) {
			 	echo $db->json(['status' => false, 'error' => $v->message]);
		 }else{
		 	$counter = 0;
		 	foreach ($_POST['child_id'] as $v) {
		 		$data = array('child_id' => $v,
		 					  'appointment_date' => $_POST['appointment_date'],
	 					   	  'appointment_time' => $_POST['appointment_time'],
	 					   	  'appoint_by' => $auth_id);

		 		$last_id = $db->saveWithLastId('tbl_appointment', $data);

		 		if ($last_id > 0) {
		 			foreach ($_POST['vac_id'] as $vaccines) {
		 				$data_details = array('appointment_id' => $last_id,
						 					  'vac_id' => $vaccines,
						 					  'child_id' => $v);
		 				// Save
						$db->save('tbl_appointment_detail', $data_details);	 		
		 				$db->log('added appoinment on '.$_POST['appointment_date'].' '.$_POST['appointment_time'], 'Appointment', $auth_id);		
		 			}
		 			$counter++;
		 		}
		 	}

		 	echo $db->json(['status' => true, 'message' => $counter.' Schedule saved successfully!']);
		 }
	break;

	case 'add_detail_vaccine':
		$i = 0;
		$e = 0;
		$v = new Validator();

		 $v->make($_POST, [
		 	'child_id' => 'required',
		 	'appointment_id' => 'required',
		 	'vac_id' => 'required'
		 ]);

		 if (!$v->fail) {
			 	echo $db->json(['status' => false, 'error' => $v->message]);
		 }else{
			$appointment_id = $_POST['appointment_id'];
			$child_id = $_POST['child_id'];
			$vac_id = $_POST['vac_id'];

			foreach ($vac_id as $vaccines) {
				$data_details = array('appointment_id' => $appointment_id,
				 					  'vac_id' => $vaccines,
				 					  'child_id' => $child_id);
 				// Save
				if ($db->count($data_details, "SELECT * from tbl_appointment_detail where appointment_id=:appointment_id and vac_id=:vac_id and child_id=:child_id") > 0) {
					$e++;
				}else{
					if ($db->save('tbl_appointment_detail', $data_details)) {
		 				$db->log('added vaccine during appoinment with Appointment ID '.$_POST['appointment_id'], 'Additional Vaccine on Appointment', $auth_id);	
						$i++;
					}
				}
			}

			$message_err = ($e > 0)? "\n\n".$e.' Vaccines already exist!' : '';
			$message_success = ($i > 0)? $i.' Vaccines added successfully!' : '';
		 	echo $db->json(['status' => true, 'message' => $message_success.$message_err]);

		 }
	break;

	case 'submit_sched_detail':
		$v = new Validator();
		// echo $db->json(['status' => true, 'message' => $_POST]);
		$arr_validator = array('height' => 'required',
		 						'weight' => 'required',
		 						'signature' => 'required');

		foreach ($_POST['detail_id'] as $d_ids) {
			$arr_validator['dossage_'.$d_ids] = 'required';
			$arr_validator['body_part_'.$d_ids] = 'required';
		}

		 $v->make($_POST, $arr_validator);

		  if (!$v->fail) {
			 	echo $db->json(['status' => false, 'error' => $v->message]);
		 }else{
		 	$sched_data = array('appointment_id' => $_POST['appointment_id'],
		 						'height' => $_POST['height'],
		 						'weight' => $_POST['weight'],
		 						'signature' => $_POST['signature'],
		 						'milestone' => $_POST['milestone'],
		 						'description' => $_POST['description'],
		 						'age' => $_POST['age'],
		 						'is_finished' => date('Y-m-d H:i:s A'));

		 	// Save the signature
		 	$sig = file_get_contents($_POST['signature']);
		 	$base_directory = '../webroot/img/signature/';
		 	$file_name = md5('signature').$_POST['appointment_id'].'.png';
		 	$filename = $base_directory.''.$file_name;
		 	file_put_contents($filename, $sig);
		 	// save signature

		 	if ($db->update('tbl_appointment', $sched_data)) {
		 		foreach ($_POST['detail_id'] as $d_ids) {
				 	$detail_id = $d_ids;
					$dossage = $_POST['dossage_'.$d_ids];
					$body_part = $_POST['body_part_'.$d_ids];
				
					  $data = array("detail_id" => $detail_id,
								"dossage" => $dossage,
								"body_part" => $body_part,
								'is_finished' => date('Y-m-d H:i:s A'),
								'user_id' => $auth_id);

					  $db->update('tbl_appointment_detail', $data);
	 				$db->log('submitted and mark appointment as finish with the appointment ID '.$_POST['appointment_id'], 'Appointment', $auth_id);

				}
		 		echo $db->json(['status' => true, 'message' => 'Appointment has been finished!']);
		 	}

		 }
	break;

	case 'add_child_death':
		$v = new Validator();

		$arr_validator = array('child_id' => 'required',
								'cause_of_death' => 'required',
		 						'death_date_time' => 'required',
		 						'signature' => 'required');

		 $v->make($_POST, $arr_validator);

		  if (!$v->fail) {
			 	echo $db->json(['status' => false, 'error' => $v->message]);
		 }else{
		 	if (!empty($_POST['death_id'])) {

		 		$data = array('death_id' => $_POST['death_id'],
						'child_id' => $_POST['child_id'],
						'cause_of_death' => $_POST['cause_of_death'],
						'death_date_time' => $_POST['death_date_time'],
						'signature' => $_POST['signature'],
						'user_id' => $auth_id,
						'updated_at' => date('Y-m-d H:i:s A'));

				save_signature($_POST['signature'], $_POST['child_id'], 'death');

		 		if ($db->update('tbl_death_record', $data)) {
		 			echo $db->json(['status' => true, 'message' => 'Deceased record has been updated!']);
	 				$db->log('added child profile as death with the child ID '.$_POST['child_id'], 'Death', $auth_id);
		 		}
		 	}else{

		 		$data = array('child_id' => $_POST['child_id'],
						'cause_of_death' => $_POST['cause_of_death'],
						'death_date_time' => $_POST['death_date_time'],
						'signature' => $_POST['signature'],
						'user_id' => $auth_id,
						'created_at' => date('Y-m-d H:i:s A'));

		 		$data_child = array('child_id' => $_POST['child_id'],
		 							'is_death' => 1);

		 		$db->update('tbl_child', $data_child);
		 		// Save the signature
				save_signature($_POST['signature'], $_POST['child_id'], 'death');
			 	// save signature

		 		if ($db->save('tbl_death_record', $data)) {
		 			echo $db->json(['status' => true, 'message' =>'New Deceased record has been saved!']);
	 				$db->log('added child profile as death with the child ID '.$_POST['child_id'], 'Death', $auth_id);
		 		}
		 	}
		 }
	break;

	default:

	break;
}
