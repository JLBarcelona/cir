<!DOCTYPE html>
<html>
<title>Forgot Password</title>
  <?php include("Layout/header.php") ?>
  <!-- Header css meta -->
<body class="" style="background-color: #f1f1f1">
  <div class="">
  <!-- navbar -->
  <?php include("Layout/nav.php") ?>
  <!-- Sidebar -->
   <section class="">
      <div class="container-fluid">
        <div class="row d-flex justify-content-center">
          <div class="col-sm-3 mt-5">
            <form class="needs-validation" id="sample_form_id" novalidate>
              <div class="card">
                <div class="card-header bg-success text-center">
                  Forgot Password
                </div>
                <div class="card-body">
                    <div class="form-row">
                      <div class="form-group col-sm-12">
                        <label>Username </label>
                        <input type="text" id="username" name="username" placeholder="Username" class="form-control " required>
                        <div class="invalid-feedback" id="err_username"></div>
                      </div>
                      <div class="col-sm-12 text-right">
                       <button class="btn btn-success" type="submit">Verify Username</button>
                      </div>
                    </div>
                </div>
                <div class="card-footer text-right">
                </div>
              </div>
            </form>
            <form class="needs-validation hide" id="asking_form" novalidate>
              <div class="card">
                <div class="card-header bg-success text-center">
                  Answer Security Question
                </div>
                <div class="card-body">
                    <div class="form-row">
                      <div class="form-group col-sm-12">
                        <label>Question </label>
                        <select class="form-control" name="security_question" id="security_question"></select>
                        <div class="invalid-feedback" id="err_security_question"></div>
                      </div>
                      <div class="form-group col-sm-12">
                        <label>Answer </label>
                        <input type="text" name="security_answer" id="security_answer" class="form-control" placeholder="Enter Answer">
                        <div class="invalid-feedback" id="err_security_answer"></div>
                      </div>
                      <div class="col-sm-12 text-right">
                       <button class="btn btn-success" type="submit">Check Answer</button>
                      </div>
                    </div>
                </div>
                <div class="card-footer text-right">
                </div>
              </div>
            </form>

            <form class="needs-validation hide" id="change_pass" novalidate>
              <div class="card">
                <div class="card-header bg-success text-center">
                  Set New Password
                </div>
                <div class="card-body">
                    <div class="form-row">
                      <input type="hidden" name="user_id" id="user_id">
                      <div class="form-group col-sm-12">
                        <label>New Password </label>
                        <input type="password" name="new_password" id="new_password" class="form-control" placeholder="Enter Answer">
                        <div class="invalid-feedback" id="err_new_password"></div>
                      </div>
                      <div class="form-group col-sm-12">
                        <label>Confirm Password </label>
                        <input type="password" name="confirm_password" id="confirm_password" class="form-control" placeholder="Enter Answer">
                        <div class="invalid-feedback" id="err_confirm_password" data-custom-validator="Please confirm your password!"></div>
                      </div>
                      <div class="col-sm-12 text-right">
                       <button class="btn btn-success" type="submit">Check Answer</button>
                      </div>
                    </div>
                </div>
                <div class="card-footer text-right">
                </div>
              </div>
            </form>

            <div class="text-center">
              <a href="index.php">Back to login.</a>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
</body>
  <!-- Footer Scripts -->
  <?php include("Layout/footer.php") ?>
</html>

<script type="text/javascript">
  $("#sample_form_id").on('submit', function(e){
    var mydata = $(this).serialize();
    e.stopPropagation();
    e.preventDefault(e);
    // alert(mydata);
    $.ajax({
      type:"POST",
      url:url + '?action=forgot_password',
      data:mydata,
      dataType:'json',
      cache:false,
      beforeSend:function(){
          //<!-- your before success function -->
      },
      success:function(response){
          console.log(response);
        if(response.status == true && response.data !== false){
          showValidator(response.error,'sample_form_id');
          $("#sample_form_id").hide();
             let dt = response.data;

              let opt = '';
              opt +='<option value="'+dt.security_answer1+'">'+dt.security_question1+'</option>';
              opt +='<option value="'+dt.security_answer2+'">'+dt.security_question2+'</option>';

          $("#user_id").val(dt.user_id);
          $("#security_question").html(opt);
          $("#asking_form").show();
        }else if (response.status == false) {
          showValidator(response.error,'sample_form_id');
        }else{
          swal("Error", "Username not found!", "error");
          showValidator(response.error,'sample_form_id');
        }
      },
      error:function(error){
        console.log(error)
      }
    });
  });


   $("#asking_form").on('submit', function(e){
    var mydata = $(this).serialize();
    e.stopPropagation();
    e.preventDefault(e);
    // alert(mydata);
    $.ajax({
      type:"POST",
      url:url + '?action=check_answer',
      data:mydata,
      dataType:'json',
      cache:false,
      beforeSend:function(){
          //<!-- your before success function -->
      },
      success:function(response){
          // console.log(response);
        if(response.status == true){
           $("#asking_form").hide();
           $("#change_pass").show();
          showValidator(response.error,'asking_form');
        }else if (response.pass == false){
          swal("Error", "Answer is not match!", "error");
          showValidator(response.error,'asking_form');
        }else{
          showValidator(response.error,'asking_form');
        }
      },
      error:function(error){
        console.log(error)
      }
    });
  });


   $("#change_pass").on('submit', function(e){
    var mydata = $(this).serialize();
    e.stopPropagation();
    e.preventDefault(e);
    // alert(mydata);
    $.ajax({
      type:"POST",
      url:url + '?action=change_pass',
      data:mydata,
      dataType:'json',
      cache:false,
      beforeSend:function(){
          //<!-- your before success function -->
      },
      success:function(response){
          console.log(response);
        if(response.status == true){
          swal("success", response.message, "success");
          showValidator(response.error,'change_pass');
          setTimeout(function(){
            window.location = 'index.php';
          },1000);
        }else if (response.pass == false){
          swal("Error", "Answer is not match!", "error");
          showValidator(response.error,'change_pass');
        }else{
          showValidator(response.error,'change_pass');
        }
      },
      error:function(error){
        console.log(error)
      }
    });
  });


</script>

