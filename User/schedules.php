<!DOCTYPE html>
<html>
<title>Schedule</title>
  <?php include("./Layout/header.php") ?>
  <link rel="stylesheet" href="../webroot/plugins/fullcalendar/main.min.css">
  <link rel="stylesheet" href="../webroot/plugins/fullcalendar-daygrid/main.min.css">
  <link rel="stylesheet" href="../webroot/plugins/fullcalendar-timegrid/main.min.css">
  <link rel="stylesheet" href="../webroot/plugins/fullcalendar-bootstrap/main.min.css">


  <!-- Header css meta -->
<body class="sidebar-mini layout-fixed" onload="active_tab('sched_tab'); show_calendar();">
  <div class="wrapper">
  <!-- navbar -->
  <?php include("./Layout/nav.php") ?>
  <!-- Sidebar -->
  <?php include("./Layout/sidebar.php") ?>
   <section class="content-wrapper">
      <div class="container-fluid">
        <div class="row pt-3">
          <div class="col-sm-12">
              <div class="card">
              <div class="card-header bg-success">
                <span class="h4"><i class="fa fa-calendar"></i> Vaccine Schedule</span>
                <button class="btn btn-sm btn-dark float-right" onclick="add_sched('');"><i class="fa fa-plus"></i></button>
              </div>
              <div class="card-body">
                <ul class="nav nav-fill" id="myTab" role="tablist">
                  <li class="nav-item active">
                    <a class="nav-link bg-primary border bold" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Calendar</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link bg-default border bold" href="index2.php" role="tab" aria-controls="profile" aria-selected="false">Tabular</a>
                  </li>
              
                </ul>
                <div class="tab-content" id="myTabContent">
                  <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                       <div id='calendar' style="cursor: pointer;"></div>
                  </div>
                  <div class="tab-pane fade " id="profile" role="tabpanel" aria-labelledby="profile-tab">

                  </div>
                </div>
             
              </div>
              <div class="card-footer"></div>
            </div>
            
          </div>
        </div>
      </div>
    </section>
  </div>
</body>

<div class="modal fade" role="dialog" id="modal_sched">
  <div class="modal-dialog modal-lg" style="max-width:95%;">
    <div class="modal-content">
      <div class="modal-header">
        <div class="modal-title sched_modal_title bold">
        
        </div>
        <button class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
       <form action="#" id="form_table_sched">
          <div class="table-responsive">
            <table class="table table-bordered dt-responsive nowrap" id="tbl_children" style="width: 100%;">
              <thead>
                <tr>
                  <th class="">
                    <input type="checkbox" id="check_all_child" class="actions" onClick="check_uncheck_checkbox(this.checked);">
                  </th>
                  <th>Fullname</th>
                  <th>Gender</th>
                  <th>CP#</th>
                  <th>Type Vaccine</th>
                  <th class="text-center">Status</th>
                  <th>
                     <!-- <button disabled="" title="Mark all checked as Finish" class="all_action btn btn-success btn-sm font-base mt-1" onclick="finish_all_check();" type="button"><i class="fa fa-check"></i> </button> -->

                     <button disabled="" title="Remove all checked from schedule" class="all_action btn btn-danger btn-sm font-base mt-1" onclick="delete_all_check();" type="button"><i class="fa fa-times"></i> </button>
                  </th>
                </tr>
              </thead>
            </table>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button class="btn btn-sm btn-primary print_btn" data-start="" data-end="" onclick="print_data();">Print Schedule</button>
      </div>
    </div>
  </div>
</div>

<!-- sorting_disabled -->
  <!-- Footer Scripts -->
  <?php include("./Layout/footer.php") ?>
  <script src="../webroot/plugins/moment/moment.min.js"></script>
  <script src="../webroot/plugins/fullcalendar/main.min.js"></script>
  <script src="../webroot/plugins/fullcalendar-daygrid/main.min.js"></script>
  <script src="../webroot/plugins/fullcalendar-timegrid/main.min.js"></script>
  <script src="../webroot/plugins/fullcalendar-interaction/main.min.js"></script>
  <script src="../webroot/plugins/fullcalendar-bootstrap/main.min.js"></script>
</html>
<!-- children script -->

  <?php include("./Schedule/sched_add.php") ?>

<script type="text/javascript">
  var tbl_children;
  var data_table_record = '';

  function show_on_sched(date_param){
     data_table_record = date_param;
    let url =  url_user+'?action=show_all_on_sched&date_sched='+ date_param;
    $.ajax({
        type:"GET",
        url:url,
        data:{},
        dataType:'json',
        beforeSend:function(){
        },
        success:function(response){
          // console.log(response);
          $(".sched_modal_title").text('List Schedule for '+'('+response.title+')');
          show_children(response.data);
          $(".print_btn").attr('data-start', date_param);
          $(".print_btn").attr('data-end', date_param);
          $("#modal_sched").modal({'backdrop' : 'static'});
        },
        error: function(error){
          console.log(error);
        }
      });
  }

  function show_children(data){
    if (tbl_children) {
      tbl_children.destroy();
    }
    tbl_children = $('#tbl_children').DataTable({
    pageLength: 10,
    responsive: true,
    data: data,
    deferRender: true,
    language: {
    "emptyTable": "No data available"
  },
    columns: [{
    className: 'text-center width-option-1 ',
    "data": "appointment_id",
    "orderable": false,
    "render": function(data, type, row, meta){

      if (row.status == '' || row.status == null) {
         return '<input type="checkbox" class="select_each" name="selected_child[]" id="selected_child'+row.appointment_id+'" value="'+row.appointment_id+'">';
      }else{
         return '<input type="checkbox" disabled>';
      }

    
    }
  },{
    className: '',
    "data": "fullname",
    "title": "Child Name",
    "render": function(data, type, row, meta){
     let mname = (row.middlename == '' || row.middlename == null)? '' : ', '+row.middlename;
     return row.fullname+mname;
    }
  },{
    className: '',
    "title": "Months/Age",
    "data": "child_age",
  },{
    className: '',
    "data": "contact_number",
  },{
    className: '',
    "data": "vacines_detail",
  },{
    className: 'text-center',
    "data": "description",
     "render": function(data, type, row, meta){
        return row.status_msg;
      }
  },{
    className: 'width-option-1 text-center',
    "data": "appointment_id",
    "orderable": false,
      "render": function(data, type, row, meta){
        var param_data = JSON.stringify(row);
        newdata = '';
          if (row.status == '' || row.status == null) {
           // newdata += ' <button title="Mark as Finish" class="btn btn-success btn-sm font-base mt-1" data-info=\' '+param_data.trim()+'\' onclick="mark_finish_appointment(\''+row.appointment_id+'\')" type="button"><i class="fa fa-check"></i> </button>';
           newdata += '<a target="_blank" href="sched_details.php?r=d210658dbb82bd59bd7ea38ed51eb283&appid='+row.appointment_id+'" title="Show Details" class="btn btn-info btn-sm font-base" ><i class="fa fa-list-alt"></i> </a>';
           newdata += ' <button title="Remove from schedule" class="btn btn-danger btn-sm font-base" data-info=\' '+param_data.trim()+'\' onclick="delete_appointment(\''+row.appointment_id+'\')" type="button"><i class="fa fa-trash"></i> </button>';
        }else{
          // newdata +='<span class="btn btn-success btn-sm">Finished</span>';
          newdata += '<a target="_blank" href="sched_details.php?r=d210658dbb82bd59bd7ea38ed51eb283&appid='+row.appointment_id+'" title="Show Details" class="btn btn-info btn-sm font-base" ><i class="fa fa-list-alt"></i> </a>';
          newdata += ' <a target="_blank" href="print_per_schedule.php?r=d210658dbb82bd59bd7ea38ed51eb283&appid='+row.appointment_id+'" title="Print" class="btn btn-primary btn-sm font-base" ><i class="fa fa-print"></i> </a>';
        }
        
        
        return newdata;
      }
    }
  ]
  });

    $(".actions").parents('th').removeClass("sorting_asc");
    $(".actions").parents('th').addClass("sorting_disabled");
    // sorting_asc 

      $('.select_each').click(function () {
         var check = $('.select_each:input:checkbox:checked').length;
          if (check > 0) {
            $(".all_action").attr('disabled', false);

          }else{
             $(".all_action").attr('disabled', true);

          }
      });


  }

</script>
  
  <script>
  var calendar;
 

  function show_calendar(){
    if (calendar) {
      calendar.destroy();
    }
    $.ajax({
        type:"GET",
        url:url_user+'?action=list_sched',
        data:{},
        dataType:'json',
        beforeSend:function(){
        },
        success:function(response){
          // console.log(response);
         if (response.status == true) {
            var calendarEl = document.getElementById('calendar');
            calendar = new FullCalendar.Calendar(calendarEl, {
            selectable: true,
            plugins: [ 'bootstrap', 'interaction', 'dayGrid', 'timeGrid' ],
            header    : {
              left  : 'prev,next today',
              center: 'title',
              right : 'dayGridMonth,timeGridWeek,timeGridDay'
            },
            'themeSystem': 'bootstrap',
            editable  : false,
            events: response.data,
            eventClick: function(info) {
              let eventObj = info.event;
               // console.log(eventObj.id);
               show_on_sched(eventObj.id);
               
            },
            dateClick: function(info) {
              add_sched(info.dateStr);
              // alert('clicked ' + info.dateStr);
            },
            select: function(info) {
              // alert('selected ' + info.startStr + ' to ' + info.endStr);
            }
            });

            calendar.render();
            // swal("Success", response.message, "success");
         }else{
          console.log(response);
         }
        },
        error: function(error){
          console.log(error);
        }
      });
  }


  function print_data(){
    let selector = $(".print_btn");
    let date_from = selector.data('start');
    let date_to = selector.data('end');
    window.location = 'print_sched.php?r=d210658dbb82bd59bd7ea38ed51eb283&from='+date_from+'&to='+date_to;
  }

</script>