<?php
require_once("../config/config.php");
require('../vendor/FPDF/html_table.php');
require_once("../config/auth.php");

$data_output = '';
$db = new db();
$auth_id = (Auth::check())?  Auth::user('firstname') .' '.Auth::user('lastname') : '';


function get_user($user_type){
	if ($user_type == 1) {
		return 'Administrator';
	}elseif ($user_type == 2) {
		return 'Midwife';
	}elseif ($user_type == 3) {
		return 'Health Worker';
	}
}

$date_from = $_GET['from'];
$date_to = $_GET['to'];


$title = ($date_from === $date_to)? date('F d, Y', strtotime($date_from)) : date('F d, Y', strtotime($date_from)).' - '. date('F d, Y', strtotime($date_to)); 

$pdf = new PDF('L','mm','A4');
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Ln(1);
$pdf->SetFont('Times','B',13);
$pdf->Cell(0,0,'CHILD IMMUNIZATION SUMMARY REPORT for the Month of ', 0,1,'C',false);
$pdf->Ln(6);
$pdf->Cell(0,0,$title,0,1,'C',false);
$pdf->Ln(7);
// $pdf->SetFont('Times','B',13);
// $pdf->Cell(0,0,'March 9, 2021',0,1,'C',false);
// $pdf->Ln(5);

$new_data = array();
$array = array('start_date' => $date_from, 'end_date' => $date_to);
$query = "SELECT a.*,b.*,concat(b.lastname,' ', b.firstname) as fullname from tbl_appointment a
 		  INNER JOIN tbl_child b on a.child_id=b.child_id 
 		  WHERE DATE(a.appointment_date) BETWEEN DATE(:start_date) AND DATE(:end_date) 
	 	  order by b.purok,b.lastname,b.firstname,b.middlename asc";
$res = $db->get($array,$query);


$data_output .='<table border="1">
		<thead>
			<tr>
				<td width="100" bgcolor="#D0D0FF"><b>Child Name</b></td>
				<td width="100" bgcolor="#D0D0FF"><b>Birthdate</b></td>
				<td width="100" bgcolor="#D0D0FF"><b>Date</b></td>
				<td width="100" bgcolor="#D0D0FF"><b>Status</b></td>
				<td width="50" bgcolor="#D0D0FF"><b>Height</b></td>
				<td width="50" bgcolor="#D0D0FF"><b>Weight</b></td>
				<td width="370" bgcolor="#D0D0FF"><b>Vaccines</b></td>
				<td width="80" bgcolor="#D0D0FF"><b>Doses</b></td>
				<td width="80" bgcolor="#D0D0FF"><b>Date Injected</b></td>
				<td width="80" bgcolor="#D0D0FF"><b>site</b></td>
			</tr>
		</thead><tbody>';
		foreach ($res as $row) {
			$dt = array();
			$dt = [];
			$array_vac = array('appointment_id' => $row->appointment_id);
			$query_vac = "SELECT a.*,b.* from tbl_appointment_detail 
			a INNER JOIN tbl_vaccine b on a.vac_id=b.vac_id
			where a.appointment_id=:appointment_id and a.is_finished is not null";

			$res_vac = $db->get($array_vac,$query_vac);
			$part = '';
			$is_finished = '';
			$doses = 0;

			$status = '';

			foreach ($res_vac as $key) {
				$dt[] = $key->vaccine_name;
				$part = $key->body_part;
				$is_finished = date('F d, Y', strtotime($key->is_finished));
				$doses++;
			}


			if(empty($row2->is_finished)){
				$status = 'No Appearance';
			}elseif (strtotime(date('Y-m-d', strtotime($row2->is_finished))) > strtotime(date('Y-m-d', strtotime($row2->appointment_date)))) {
				$status = 'No Late';		
			}else{
				$status = 'On Time';		
			}

			$parts = (!empty($part)) ? $part : 'No Data';
			$vaccines = (!empty($dt))? implode(',', $dt) : 'No Data';
			$vac_date = (!empty($is_finished))? $is_finished : 'No Data';
			$remarks = (!empty($row2->description))? $row2->description : 'No Data';


			$date = date('Y');
			$bdate = date('Y', strtotime($row->birthdate));

			$mdate = date('m');
			$mbdate = date('m', strtotime($row->birthdate));

			$m = abs($mdate - $mbdate);
			$y = abs($date - $bdate);

			$year = ($y < 0)? 0 : $y;
			$month = ($year > 0)? '' : ($m > 0)? '.'.$m : '';

			$prefix = ($year == 0)? ' Month(s) Old' : ' Year(s) Old';

			$height = (!empty($row->height))? $row->height : 'No Data';
			$weight = (!empty($row->weight))? $row->weight : 'No Data';

			$age = $year.$month.$prefix;

				$data_output .= '<tr>
								<td width="100">'.$row->fullname.'</td>
								<td width="100">'.date('F d, Y', strtotime($row->birthdate)).'</td>
								<td width="100">'.date('F d, Y', strtotime($row->appointment_date)).'</td>
								<td width="100">'.$status.'</td>
								<td width="50">'.$height.'</td>
								<td width="50">'.$weight.'</td>
								<td width="370">'.$vaccines.'</td>
								<td width="80">'.$doses.' of '.$doses.'</td>
								<td width="80">'.$vac_date.'</b></td>
								<td width="80">'.$parts.'</td>
							</tr>';




			
		}
$data_output .='</tbody>';
$data_output .= '</table>';


// data_output

$pdf->SetFont('Times','',8);
$pdf->WriteHTML($data_output);
$pdf->Ln(10);
$pdf->SetFont('Times','B',10);
// $pdf->Image($base_directory,125,185,100);
$pdf->Cell(0,0,'Prepared by: '.$auth_id,0,1,'R',false);
$pdf->Ln(5);
$pdf->Cell(0,0,get_user(Auth::user('user_type')),0,1,'R',false);
// $pdf->AddPage();
// $pdf->WriteHTML($crop);
$pdf->Output();
?>
