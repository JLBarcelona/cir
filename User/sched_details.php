<?php 
if (md5('sched details') !== $_REQUEST['r']) {
  @header('location:./index.php');
}

?>
<!DOCTYPE html>
<html>
  <title>Children</title>
  <?php include("./Layout/header.php") ?>
  <!-- Header css meta -->
<body class="sidebar-mini layout-fixed" onload="active_tab('children_tab'); show_table_details();">
<script src="../webroot/Signature/signature.js"></script>
  <div class="wrapper">
  <!-- navbar -->
  <?php include("./Layout/nav.php") ?>
  <!-- Sidebar -->
  <?php include("./Layout/sidebar.php") ?>
   <section class="content-wrapper" data-id="<?php echo $_REQUEST['appid'] ?>">
      <div class="container-fluid">
        <div class="row pt-3">
          <div class="col-sm-12">  
            <div class="alert alert-primary shadow-sm"><i class="fa fa-calendar"></i> Scheduled on <span class="appointment_date bold"></span></div>
            <div class="card shadow-sm">
              <div class="card-header bg-success">
                <span class="h4"><i class="fa fa-baby"></i> Child Information</span>
              </div>
              <div class="card-body">
                <ul class="list-group">
                  <li class="list-group-item"><b>Name:</b> <span class="child_name"></span></li>
                  <li class="list-group-item"><b>Birthdate:</b> <span class="birthdate"></span></li>
                  <li class="list-group-item"><b>Father's Name:</b> <span class="father_name"></span></li>
                  <li class="list-group-item"><b>Mother's Name:</b> <span class="mother_name"></span></li>
                  <li class="list-group-item"><b>Address:</b> <span class="address"></span></li>
                </ul>
              </div>
              <div class="card-footer"></div>
            </div>

             <form action="#" id="form_details_vaccine" class="needs-validation" novalidate>
              <input type="hidden" name="appointment_id" placeholder="" class="form-control" required value="<?php echo $_REQUEST['appid'] ?>">
                <div class="card shadow-sm">
                  <div class="card-header bg-success">
                    <span class="h4"><i class="fa fa-syringe"></i> Vaccines</span>
                    <button class="float-right btn btn-dark" onclick="add_vaccine_sched();" type="button"><i class="fa fa-plus"></i></button>
                  </div>
                  <div class="card-body">
                        <table class="table table-bordered dt-responsive nowrap" id="tbl_appoint" style="width: 100%;"></table>
                    
                  </div>
                  <div class="card-footer">
                  </div>
                </div>


                <div class="card shadow-sm">
                  <div class="card-header bg-success">
                    <span class="h4"><i class="fa fa-list-alt"></i> Appointment Details</span>
                  </div>
                    <div class="card-body">
                      <div class="row">
                        <div class="col-sm-4 form-group">
                          <label>Age</label>
                          <input type="text" class="form-control" name="age" id="age">
                          <div class="invalid-feedback" id="err_age"></div>
                        </div>
                        <div class="col-sm-4 form-group">
                          <label for="">Height(cm)</label>
                          <input type="text" class="form-control" placeholder="Height" id="height" name="height">
                          <div class="invalid-feedback" id="err_height"></div>
                        </div>

                        <div class="col-sm-4 form-group">
                          <label for="">Weight(kg)</label>
                          <input type="text" class="form-control" placeholder="Weight" id="weight" name="weight">
                          <div class="invalid-feedback" id="err_weight"></div>
                        </div>

                        <div class="col-sm-12 form-group">
                          <div class="row ">
                            <div class="col-sm-2">
                                 <div>
                                  <label for="">Milestone (optional)</label>
                                    <div>
                                      <img src="../webroot/img/img.png" style="background-image: url('../webroot/img/default.jpg');" class="img-bg img-show-full img-fluid border" data-src="../webroot/img/default.jpg" alt="User Image" width="320" id="milestone_preview">
                                    </div>
                                      <button class="btn mt-2 btn-success btn-sm btn-block" type="button" onclick="show_upload('milestone');">Upload Milestone</button>
                                      <textarea class="hide" name="milestone" id="milestone"></textarea>
                                </div>
                            </div>
                            <div class="col-sm-10">
                              <label for="">&nbsp;</label>
                              <textarea class="form-control" name="description" id="description" placeholder="Write some milestone description..." rows="13"></textarea>
                            </div>
                          </div>
                        </div>



                        <div class="col-sm-12 form-group">
                           <label>Signature</label>
                           <textarea name="signature" id="signature" class="form-control hide"></textarea>
                            <div class="invalid-feedback" id="err_signature"></div>
                          <div class="border">
                            <canvas id="signature-pad" class="signature-pad" style="width: 100%; height:250px;" onmouseup="get_source();" on ontouchend="get_source();"></canvas>
                          </div>
                        </div>
                      </div>
                    </div>
                  <div class="card-footer text-right">
                    <button class="btn btn-dark float-left mr-1" id="undo" type="button">Undo</button>
                     <button class="btn btn-dark float-left" id="clear" type="button">Clear</button>
                    <button class="btn btn-success" type="submit">Mark as Finish</button>
                    <a href="./print_per_schedule.php?r=d210658dbb82bd59bd7ea38ed51eb283&appid=<?php echo $_REQUEST['appid'] ?>" class="btn btn-primary hide print_btn">Print</a>
                    <div class="btn btn-dark edit-form hide" onclick="edit_able();" type="submit">Edit Form</div>
                    <div class="btn btn-dark cancel-form hide" onclick="edit_disable();" type="submit">Cancel Edit</div>
                  </div>
                </div>
            </form>

          </div>
        </div>
      </div>
    </section>
  </div>

  
      <div class="modal fade" role="dialog" id="modal_upload">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <div class="modal-title">
                Upload
              </div>
              <a href="#" class="close" data-dismiss="modal">&times;</a>
            </div>
            <div class="modal-body">
              <input type="hidden" name="input" id="input">
              <div class="text-center">
                <div id="preview_img"></div>
              </div>
              <div class="custom-file">
                <input type="file" class="custom-file-input" onchange="previewFile();" accept="image/*" id="file" name="file" required>
                <label class="custom-file-label" for="validatedCustomFile">Choose file</label>
              </div>
            </div>
            <div class="modal-footer text-right">
              <button class="btn btn-default" onclick="close();" data-dismiss="modal">Close</button>
              <button class="btn btn-dark" onclick="upload();">Upload</button>
            </div>
          </div>
        </div>
      </div>

<div class="modal fade" role="dialog" id="add_sched_detail_modal">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <div class="modal-title">
            <span class="child_name bold h5"></span>
          </div>
          <button class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <form class="needs-validation" id="detail_form" action="#" novalidate>
            <div class="form-row">
              <input type="hidden" id="appointment_id" name="appointment_id" placeholder="" class="form-control" required value="<?php echo $_REQUEST['appid'] ?>">
              
                <div class="form-group col-sm-12">
                  <!-- <label>Children </label> -->

                  <input type="hidden" name="child_id" id="child_id" readonly="">
                  <div class="invalid-feedback" id="err_child_id" data-custom-validator="Child name is required!"></div>
                </div>

                <div class="form-group col-sm-12">
                  <label>Vaccine </label>
                  <div class="input-group mb-3">
                   <select id="vac_id" name="vac_id[]" class="form-control vac_id" multiple="multiple" data-placeholder="Add Vaccine"></select>
                    <div class="input-group-append">
                      <button class="btn btn-outline-secondary" type="button" onclick="add_vaccine();"><i class="fa fa-plus"></i></button>
                    </div>
                    <div class="invalid-feedback" id="err_vac_id" data-custom-validator="Vaccine is required!"></div>
                  </div>
                </div>
              <div class="col-sm-12 text-right">
                <button class="btn btn-success" type="submit">Submit</button>
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          
        </div>
      </div>
    </div>
  </div>


  </body>
    <!-- Footer Scripts -->
    <?php include("./Layout/footer.php") ?>
    <?php include("./Vaccine/vaccine_add.php") ?>
    <script src="../webroot/js/upload.js"></script>


  </html>

  <script type="text/javascript">

    function add_vaccine_sched(){
      $("#add_sched_detail_modal").modal({'backdrop': 'static'});
      $('#vac_id').val('');
      $('#vac_id').trigger('change');
    }


    function show_child_details(){
      let id = $(".content-wrapper").data('id');
      $.ajax({
          type:"GET",
          url:url_user+'?action=get_appointment_child',
          data:{id:id},
          dataType:'json',
          beforeSend:function(){
          },
          success:function(response){
            // console.log(response);
           if (response.status == true) {
            let res = response.data;
              let is_finished = res.is_finished;
              // swal("Success", response.message, "success");
              let addr = 'Purok#'+res.purok+', '+res.street+", "+res.barangay+", "+res.address;
              $(".child_name").text(res.firstname+' '+res.lastname);
              $(".father_name").text(res.father_name);
              $(".mother_name").text(res.mother_name);
              $(".birthdate").text(res.birthdate);
              $(".age").text(response.age);
              $("#age").val(response.age);
              $(".appointment_date").text(response.appointment_date);
              $(".address").text(addr);
              $("#child_id").val(res.child_id);
              $("#signature").val(res.signature);
              $("#height").val(res.height);
              $("#weight").val(res.weight);
              $("#description").val(res.description);

              // console.log(res.milestone);

              if (res.milestone == '' || res.milestone == null ||  res.milestone == 'null') {
                
              }else{
                $("#milestone_preview").attr('style', 'background-image:url(\''+res.milestone+'\')');
                $("#milestone_preview").attr('data-src', res.milestone);
                // $("#description").val(res.description);
                $("#milestone").val(res.milestone);
              }
              
              init_signature();
              init_form(res.is_finished);
              if (is_finished == '' || is_finished === null) {
                $(".edit-form").hide();
                $(".cancel-form").hide();
                // $(".print_btn").removeClass('hide');
                edit_able();
              }else{
                
              }
           }else{
            console.log(response);
           }
          },
          error: function(error){
            console.log(error);
          }
        });
    }
  </script>

  <!-- Javascript Function-->
  <script>
  $("#form_details_vaccine").on('submit', function(e){
    e.preventDefault();
    let data = $(this).serialize();
    let url = url_user + '?action=submit_sched_detail';

     $.ajax({
        type:"POST",
        url:url,
        data:data,
        dataType:'json',
        beforeSend:function(){
        },
        success:function(response){
          // console.log(response);
         if (response.status == true) {
            swal("Success", response.message, "success");
            showValidator(response.error,'form_details_vaccine');
            setTimeout(function(){
              location.reload();
            },1000);
         }else{
          console.log(response);
            showValidator(response.error,'form_details_vaccine');
         }
        },
        error: function(error){
          console.log(error);

        }
      });
  });

  var tbl_appoint;
  function show_appoint(datas){
    if (tbl_appoint) {
      tbl_appoint.destroy();
    }
    tbl_appoint = $('#tbl_appoint').DataTable({
    pageLength: 10,
    responsive: true,
    data: datas,
    deferRender: true,
    language: {
    "emptyTable": "No data available"
  },
    columns: [{
    className: '',
    "data": "vaccine_name",
    "title": "Vaccine",
  },{
    className: '',
    "data": "detail_id",
    "title": "Dossage",
    "render": function(data, type, row, meta){
        var param_data = JSON.stringify(row);
        // console.log(row);
        newdata = '';
        let dossage = (row.dossage == '' || row.dossage === null)? '' : row.dossage;

        newdata += '<input type="hidden" class="form-control" name="detail_id[]" value="'+row.detail_id+'">';
        newdata += '<input type="text" class="form-control validate_input" name="dossage_'+row.detail_id+'" id="dossage_'+row.detail_id+'" oninput="validate_input_manual();" placeholder="Dossage" value="'+dossage+'">';
        newdata += '<div class="invalid-feedback" id="err_dossage_'+row.detail_id+'" data-custom-validator="Dossage is required!"></div>';
        return newdata;
      }
  },{
    className: '',
    "data": "detail_id",
    "title": "Site of injection",
    "render": function(data, type, row, meta){
        var param_data = JSON.stringify(row);
        // console.log(row);
        let body_part = (row.body_part == '' || row.body_part === null)? '' : row.body_part;

        newdata = '';
        newdata += '<input type="text" class="form-control validate_input" name="body_part_'+row.detail_id+'" id="body_part_'+row.detail_id+'" oninput="validate_input_manual();" placeholder="Body Part" value="'+body_part+'">';
        newdata += '<div class="invalid-feedback" id="err_body_part_'+row.detail_id+'" data-custom-validator="Body part is required!"></div>';
        return newdata;
      }
  },{
    className: 'width-1 text-center',
    "data": "detail_id",
    "orderable": false,
    "title": "Options",
      "render": function(data, type, row, meta){
        var param_data = JSON.stringify(row);
        // console.log(row);
        newdata = '';
        newdata += ' <button type="button" class="btn btn-danger btn-sm font-base mt-1" data-info=\' '+param_data.trim()+'\' onclick="delete_appoint(this)" type="button"><i class="fa fa-times"></i></button>';
        return newdata;
      }
    }
  ]
  });
  }

  function show_table_details(){
    let id = $(".content-wrapper").data('id');
    var url = url_user + '?action=get_appointment_details&uid='+id;
    $.ajax({
        type:"GET",
        url:url,
        data:{id:id},
        dataType:'json',
        beforeSend:function(){
        },
        success:function(response){
          // console.log(response);
         if (response.status == true) {
            // swal("Success", response.message, "success");
            show_appoint(response.data);
         }else{
          console.log(response);
         }
        },
        error: function(error){
          console.log(error);
        }
      });

    show_child_details();
  }


  $("#detail_form").on('submit', function(e){
      var url = url_user+'?action=add_detail_vaccine';
      var mydata = $(this).serialize();
      e.stopPropagation();
      e.preventDefault(e);

      $.ajax({
        type:"POST",
        url:url,
        data:mydata,
        cache:false,
        dataType:'json',
        beforeSend:function(){
            //<!-- your before success function -->
        },
        success:function(response){
            //console.log(response)
          if(response.status == true){
            // console.log(response)
            swal("Success", response.message, "success");
            showValidator(response.error,'detail_form');
            show_table_details();
            $("#add_sched_detail_modal").modal('hide');
          }else{
            //<!-- your error message or action here! -->
            showValidator(response.error,'detail_form');
          }
        },
        error:function(error){
          console.log(error)
        }
      });
    });

  function delete_appoint(_this){
    var data = JSON.parse($(_this).attr('data-info'));
    var detail_id =  data.detail_id;
    var url =  url_user + '?action=delete_appointment_detail';
      swal({
        title: "Are you sure?",
        text: "Do you want to delete this appoint?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        closeOnConfirm: false
      },
      function(){
        $.ajax({
        type:"POST",
        url:url,
        data:{detail_id:detail_id},
        dataType:'json',
        beforeSend:function(){
      },
      success:function(response){
        // console.log(response);
        if (response.status == true) {
          swal("Success", response.message, "success");
          show_table_details();
        }else{
          console.log(response);
        }
      },
      error: function(error){
        console.log(error);
      }
      });
    });
  }

  function edit_appoint(_this){
    var data = JSON.parse($(_this).attr('data-info'));
    $('#detail_id').val(data.detail_id);
    $('#dossage').val(data.dossage);
    $('#body_part').val(data.body_part);
  }


    show_vaccines();
    $('.vac_id').select2({
      theme: 'bootstrap4',
    });

  function edit_able(){
    $(':input').attr('disabled', false); 
    $(".edit-form").addClass('hide');
    $(".print_btn").addClass('hide');
    $(".cancel-form").removeClass('hide');

  }

  function edit_disable(){
    $(':input').attr('disabled', true); 
    $(".edit-form").removeClass('hide');
    $(".cancel-form").addClass('hide');
    $(".print_btn").removeClass('hide');
  }
</script>

  <script src="../webroot/js/signatures.js"></script>