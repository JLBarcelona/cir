<?php
session_start();
require_once("../config/config.php");
require('../vendor/FPDF/html_table.php');

$db = new db();

$id = $_GET['appid'];

$array = array('appointment_id' => $id);
$query = "SELECT a.*,b.*, concat(c.firstname,' ',c.lastname) as appointed_by from tbl_appointment a 
INNER JOIN tbl_child b on a.child_id=b.child_id
INNER JOIN tbl_users c on a.appoint_by=c.user_id
where a.appointment_id=:appointment_id";

$res = $db->find($array,$query);
$child_id = $res->child_id;

$date = date('Y');
$bdate = date('Y', strtotime($res->birthdate));

$mdate = date('m');
$mbdate = date('m', strtotime($res->birthdate));

$m = abs($mdate - $mbdate);
$y = abs($date - $bdate);

$year = ($y < 0)? 0 : $y;
$month = ($year > 0)? '' : ($m > 0)? '.'.$m : '';

$prefix = ($year == 0)? ' Month(s) Old' : ' Year(s) Old';

$age = $year.$month.$prefix;

$appointment_date = date('F d, Y', strtotime($res->appointment_date));
$mname = (!empty($res->middlename)) ? ucfirst($res->middlename) : '';
$child_name = ucfirst($res->lastname).' '.ucfirst($res->firstname).', '.$mname;
$addr = 'Purok# '.$res->purok.' '.$res->street.' '.$res->barangay.', '.$res->address;
$status = 'testing';
$data_output = '';
$base_directory = '../webroot/img/signature/'.md5('signature').$id.'.png';


$array_vac = array('child_id' => $child_id);
$query_vac = "SELECT a.*,b.* from tbl_appointment_detail 
a INNER JOIN tbl_vaccine b on a.vac_id=b.vac_id
where a.child_id=:child_id";

$res_vac = $db->get($array_vac,$query_vac);


$pdf = new PDF('L','mm','A4');
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Ln(1);
$pdf->SetFont('Times','B',13);
$pdf->Cell(0,0,'INDIVIDUAL LEDGER REPORT SCHEDULE'.$appointment_date,0,1,'C',false);
$pdf->Ln(7);
// $pdf->SetFont('Times','B',13);
// $pdf->Cell(0,0,'March 9, 2021',0,1,'C',false);
// $pdf->Ln(5);


$data_output .='<table border="1">
		<thead>
			<tr>
				<td width="100" bgcolor="#D0D0FF"><b>Child Name</b></td>
				<td width="260">'.$res->lastname.' '.$res->firstname.', '.$res->middlename.'</td>
				<td width="100" bgcolor="#D0D0FF"><b>Birthdate</b></td>
				<td width="100">'.date('F d Y', strtotime($res->birthdate)).'</td>
				<td width="100" bgcolor="#D0D0FF"><b>Age</b></td>
				<td width="100">'.$age.'</td>
			</tr>
			<tr>
				<td width="100" bgcolor="#D0D0FF"><b>Father\'s Name</b></td>
				<td width="260">'.$res->father_name.'</td>
				<td width="100" bgcolor="#D0D0FF"><b>Mother\'s Name</b></td>
				<td width="300">'.$res->mother_name.'</td>
			</tr>
			<tr>
				<td width="100" bgcolor="#D0D0FF"><b>Address</b></td>
				<td width="660">'.$addr.'</td>
			</tr>
		</thead>';

$data_output .='<tbody></tbody>';
$data_output .= '</table>';



$data_output .='<table border="0">
	<thead>
		<tr>
			<td><b>Child Statistics</b></td>
		</tr>
	</thead>
</table>';


$data_output .='<table border="1">
		<thead>
			<tr>
				<td width="100" bgcolor="#D0D0FF"><b>Height</b></td>
				<td width="260">'.$res->height.'</td>
				<td width="100" bgcolor="#D0D0FF"><b>Weight</b></td>
				<td width="300">'.$res->weight.'</td>
			</tr>
			<tr>
				<td width="100" bgcolor="#D0D0FF"><b>Note</b></td>
				<td width="660">'.((!empty($res->description))? $res->description : 'N/A').'</td>
			</tr>
		</thead>';

$data_output .='<tbody></tbody>';
$data_output .= '</table>';

$data_output .='<table border="0">
	<thead>
		<tr></tr>
		<tr>
			<td><b>Vaccines</b></td>
		</tr>
	</thead>
</table>';


$data_output .='<table border="1">
		<thead>
			<tr>
				<td width="306" bgcolor="#D0D0FF"><b>Vaccine</b></td>
				<td width="100" bgcolor="#D0D0FF"><b>Dossage</b></td>
				<td width="200" bgcolor="#D0D0FF"><b>Body Part</b></td>
				<td width="153" bgcolor="#D0D0FF"><b>Date Injected</b></td>

			</tr>
		</thead><tbody>';
		foreach ($res_vac as $row) {
			if ($row->appointment_id == $id) {
				$data_output .= '<tr>
								<td  bgcolor="#D0D0aa" width="306">'.$row->vaccine_name.'</td>
								<td  bgcolor="#D0D0aa" width="100">'.$row->dossage.'</td>
								<td  bgcolor="#D0D0aa" width="200">'.$row->body_part.'</td>
								<td  bgcolor="#D0D0aa" width="153">'.$row->is_finished.'</td>
							</tr>';
			}else{
				$data_output .= '<tr>
								<td width="306">'.$row->vaccine_name.'</td>
								<td width="100">'.$row->dossage.'</td>
								<td width="200">'.$row->body_part.'</td>
								<td width="153">'.$row->is_finished.'</td>
							</tr>';
			}
			
		}
$data_output .='</tbody>';
$data_output .= '</table>';


// data_output

$pdf->SetFont('Times','',8);
$pdf->WriteHTML($data_output);
$pdf->Ln(10);
$pdf->SetFont('Times','B',10);
// $pdf->Image($base_directory,125,185,100);
$pdf->Cell(0,0,'Prepared by: '.$res->appointed_by,0,1,'R',false);
// $pdf->AddPage();
// $pdf->WriteHTML($crop);
$pdf->Output();
?>
