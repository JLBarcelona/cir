<div class="modal fade" role="dialog" id="modal_vaccine">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <div class="modal-title vaccine-modal-title">
        Add Vaccine
        </div>
        <button class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
       <form class="needs-validation" id="vaccine_form" action="#" novalidate>
          <div class="form-row">
            <input type="hidden" id="vac_id" name="vac_id" placeholder="" class="form-control" required>
            <div class="form-group col-sm-12">
              <label>Name </label>
              <input type="text" id="vaccine_name" name="vaccine_name" placeholder="Vaccine name" class="form-control " required>
              <div class="invalid-feedback" id="err_vaccine_name"></div>
            </div>

            <div class="form-group col-sm-12">
              <label>Doses </label>
              <input type="text" id="doses" name="doses" placeholder="Doses" class="form-control " required>
              <div class="invalid-feedback" id="err_doses"></div>
            </div>

            <div class="form-group col-sm-12">
              <label>Minimum Age requirement </label>
              <input type="text" id="minimum_age" name="minimum_age" placeholder="Minimum Age requirement" class="form-control " required>
              <div class="invalid-feedback" id="err_minimum_age"></div>
            </div>

            <div class="form-group col-sm-12">
              <label>Description </label>
              <textarea type="text" id="description" name="description" placeholder="Description" class="form-control " required></textarea>
              <div class="invalid-feedback" id="err_description"></div>
            </div>

            <div class="col-sm-12 text-right">
              <button class="btn btn-success" type="submit">Submit</button>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
    function add_vaccine(){
        $(".vaccine-modal-title").text('Add Vaccine');
        $("#modal_vaccine").modal({'backdrop' : 'static'});
        $("#vac_id").val('');
        $("#vaccine_name").val('');
      }

   $("#vaccine_form").on('submit', function(e){
      var url = url_user+'?action=add_vaccine';
      var mydata = $(this).serialize();
      e.stopPropagation();
      e.preventDefault(e);

      $.ajax({
        type:"POST",
        url:url,
        data:mydata,
        dataType:'json',
        cache:false,
        beforeSend:function(){
            //<!-- your before success function -->
        },
        success:function(response){
            // console.log(response);
          if(response.status == true){
            // console.log(response)
            show_vaccines();
            $("#modal_vaccine").modal('hide');
            swal("Success", response.message, "success");
            showValidator(response.error,'vaccine_form');
          }else{
            //<!-- your error message or action here! -->
            showValidator(response.error,'vaccine_form');
          }
        },
        error:function(error){
          console.log(error)
        }
      });
    });

    function delete_vaccines(_this){
      var vac_id = JSON.parse($(_this).attr('data-id'));
      // alert(vac_id);
        swal({
          title: "Are you sure?",
          text: "Do you want to delete this vaccine?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes",
          closeOnConfirm: false
        },
        function(){
          $.ajax({
          type:"POST",
          url:url_user+'?action=delete_vaccines',
          data:{vac_id : vac_id},
          dataType:'json',
          beforeSend:function(){
        },
        success:function(response){
          // console.log(response);
          if (response.status == true) {
            show_vaccines();
            swal("Success", response.message, "success");
          }else{
            console.log(response);
          }
        },
        error: function(error){
          console.log(error);
        }
        });
      });
    }

    function edit_vaccines(_this){
      var data = JSON.parse($(_this).attr('data-info'));
      $('#vac_id').val(data.vac_id);
      $('#vaccine_name').val(data.vaccine_name);
      $("#doses").val(data.doses);
      $("#minimum_age").val(data.minimum_age);
      $("#description").val(data.description);
       $(".vaccine-modal-title").text('Edit Vaccine');
      $("#modal_vaccine").modal({'backdrop' : 'static'});
    }
</script>