<!DOCTYPE html>
<html>
  <title>Health Worker</title>
  <?php include("./Layout/header.php") ?>
  <!-- Header css meta -->
<body class="sidebar-mini layout-fixed" onload="active_tab('health_worker_tab'); active_tree('account_tab','account_tree'); show_user();">
  <div class="wrapper">
  <!-- navbar -->
  <?php include("./Layout/nav.php") ?>
  <!-- Sidebar -->
  <?php include("./Layout/sidebar.php") ?>
   <section class="content-wrapper">
      <div class="container-fluid">
        <div class="row pt-3">
          <div class="col-sm-12">  
            <div class="card">
              <div class="card-header bg-success">
                <span class="h4"><i class="fa fa-user-md"></i> Health Worker's Account</span>
                <button class="btn btn-sm btn-dark float-right" onclick="add_worker();"><i class="fa fa-plus"></i></button>
              </div>
              <div class="card-body">
                <table class="table table-bordered dt-responsive nowrap" id="tbl_user" style="width: 100%;"></table>
              </div>
              <div class="card-footer"></div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
</body>


 <div class="modal fade" role="dialog" id="modal_add_midwife">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <div class="modal-title h4 midwife-modal-title">
            Add Health Worker
            </div>
            <button class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
            <form class="needs-validation" id="sample_form_id" action="#" novalidate>
              <div class="form-row">
                <input type="hidden" id="user_id" name="user_id" placeholder="" class="form-control" required>
                <div class="form-group col-sm-12">
                  <label>Firstname </label>
                  <input type="text" id="firstname" name="firstname" placeholder="Firstname" class="form-control " required>
                  <div class="invalid-feedback" id="err_firstname"></div>
                </div>
                <div class="form-group col-sm-12">
                  <label>Lastname </label>
                  <input type="text" id="lastname" name="lastname" placeholder="Lastname" class="form-control " required>
                  <div class="invalid-feedback" id="err_lastname"></div>
                </div>
                <div class="form-group col-sm-6">
                  <label>Birthdate </label>
                  <input type="date" id="birthdate" name="birthdate" placeholder="birthdate" class="form-control " required>
                  <div class="invalid-feedback" id="err_birthdate"></div>
                </div>
                  <div class="form-group col-sm-6">
                    <label>Gender </label>
                    <select id="gender" name="gender" class="form-control ">
                      <option value="" selected="">Select Gender</option>
                      <option value="Male">Male</option>
                      <option value="Female">Female</option>
                    </select>
                    <div class="invalid-feedback" id="err_gender"></div>
                  </div>
                  <div class="form-group col-sm-12">
                  <label>Address </label>
                  <input type="text" id="address" name="address" placeholder="Address" class="form-control " required>
                  <div class="invalid-feedback" id="err_address"></div>
                </div>

                <div class="col-sm-12">
                  <div class="row">
                    <div class="col-sm-3"><hr></div>
                    <div class="col-sm-6 bold h5 text-center">Security Questions</div>
                    <div class="col-sm-3"><hr></div>
                  </div>
                </div>
                
                <div class="form-group col-sm-12">
                  <label>Security Question 1</label>
                  <select id="security_question1" name="security_question1" class="form-control ">
                    <option value="" selected="">Select Question</option>
                    <option>What is your father's nickname?</option>
                    <option>What is your favorite color?</option>
                    <option>What is your pet name?</option>
                    <option>What is the brand of your first Phone?</option>
                    <option>What is your Besrfriend nickname?</option>
                  </select>
                  <div class="invalid-feedback" id="err_security_question1"></div>
                </div>
                
                <div class="form-group col-sm-12">
                  <label>Security Answer1 </label>
                  <input type="text" id="security_answer1" name="security_answer1" placeholder="Security Answer 1" class="form-control " required>
                  <div class="invalid-feedback" id="err_security_answer1"></div>
                </div>
                
                <div class="form-group col-sm-12">
                  <label>Security Question2 </label>
                  <select id="security_question2" name="security_question2" class="form-control ">
                    <option value="" selected="">Select Question</option>
                    <option>At what age did you get your first job?</option>
                    <option>What is your nickname?</option>
                    <option>What is your Mother's maden name?</option>
                    <option>In which country do you want to visit?</option>
                    <option>What is your dream car?</option>
                  </select>
                  <div class="invalid-feedback" id="err_security_question2"></div>
                </div>
                
                <div class="form-group col-sm-12">
                  <label>Security Answer2 </label>
                  <input type="text" id="security_answer2" name="security_answer2" placeholder="Security Answer 2" class="form-control " required>
                  <div class="invalid-feedback" id="err_security_answer2"></div>
                </div>
                
                 <div class="col-sm-12">
                  <div class="row">
                    <div class="col-sm-3"><hr></div>
                    <div class="col-sm-6 bold h5 text-center">Login Access</div>
                    <div class="col-sm-3"><hr></div>
                  </div>
                </div>
                
                <div class="form-group col-sm-12">
                  <label>Username </label>
                  <input type="text" id="username" name="username" placeholder="Username" class="form-control " required>
                  <div class="invalid-feedback" id="err_username"></div>
                </div>
                <div class="form-group col-sm-12 password">
                  <label>Password </label>
                  <input type="password" id="password" name="password" placeholder="Password" class="form-control " required>
                  <div class="invalid-feedback" id="err_password"></div>
                </div>

                <div class="form-group col-sm-12">
                  <input type="hidden" value="3" id="user_type" name="user_type" placeholder="" class="form-control " required>
                  <div class="invalid-feedback" id="err_user_type"></div>
                </div>

                <div class="col-sm-12 text-right">
                  <button class="btn btn-success" type="submit">Submit</button>
                </div>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            
          </div>
        </div>
      </div>
    </div>

  <!-- Footer Scripts -->
  <?php include("./Layout/footer.php") ?>
</html>

<!-- Javascript Function-->
<script>
  function add_worker(){
  $("#modal_add_midwife").modal({'backdrop' : 'static'});
  $(".midwife-modal-title").text('Add Health Worker');
  $('#user_id').val('');
  $('#firstname').val('');
  $('#lastname').val('');
  $('#birthdate').val('');
  $('#gender').val('');
  $('#address').val('');
  $('#username').val('');
  $('.password').removeClass('hide');
  $('#password').val('');
  $('#user_type').val(3);
  $("#security_question1").val('');
  $("#security_answer1").val('');
  $("#security_question2").val('');
  $("#security_answer2").val('');
  }

  var tbl_user;
  function show_user(){
    if (tbl_user) {
      tbl_user.destroy();
    }
    var url = main_path + '/user/list_user';
    tbl_user = $('#tbl_user').DataTable({
    pageLength: 10,
    responsive: true,
    ajax: url_user+'?action=health_worker_list',
    deferRender: true,
    language: {
    "emptyTable": "No data available"
  },
    columns: [{
    className: '',
    "data": "lastname",
    "title": "Name",
    "render": function(data, type, row, meta){
      return row.lastname+', '+row.firstname;
    }
  },{
    className: '',
    "data": "lastname",
    "title": "Lastname",
  },{
    className: '',
    "data": "birthdate",
    "title": "Birthdate",
  },{
    className: '',
    "data": "gender",
    "title": "Gender",
  },{
    className: '',
    "data": "address",
    "title": "Address",
  },{
    className: '',
    "data": "username",
    "title": "Username",
  },{
    className: 'width-option-1 text-center',
    "data": "user_id",
    "orderable": false,
    "title": "Options",
      "render": function(data, type, row, meta){
        newdata = '';
        newdata += '<button class="btn btn-success btn-sm font-base mt-1" data-id="'+row.user_id+'" onclick="edit_user(this)" type="button"><i class="fa fa-edit"></i> Edit</button>';
        newdata += ' <button class="btn btn-danger btn-sm font-base mt-1" data-id="'+row.user_id+'" data-name="'+row.firstname+' '+row.lastname+'" onclick="delete_user(this)" type="button"><i class="fa fa-edit"></i> Delete</button>';
        return newdata;
      }
    }
  ]
  });
  }

  $("#sample_form_id").on('submit', function(e){
    // var url = $(this).attr('action');
    var mydata = $(this).serialize();
    e.stopPropagation();
    e.preventDefault(e);
    // console.log(mydata);
    $.ajax({
      type:"POST",
      url:url_user+'?action=add_user',
      data:mydata,
      cache:false,
      dataType: 'json',
      beforeSend:function(){
          //<!-- your before success function -->
      },
      success:function(response){
          // console.log(response);
        if(response.status == true){
          // console.log(response);
          show_user();
          swal("Success", response.message, "success");
          $("#modal_add_midwife").modal('hide');
          showValidator(response.error,'sample_form_id');
        }else{
          //<!-- your error message or action here! -->
          showValidator(response.error,'sample_form_id');
        }
      },
      error:function(error){
        console.log(error)
      }
    });
  });

  function delete_user(_this){
    var id = $(_this).attr('data-id');
    var name = $(_this).attr('data-name');
      swal({
        title: "Are you sure?",
        text: "Do you want to delete "+name+"?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        closeOnConfirm: false
      },
      function(){
        $.ajax({
        type:"POST",
        url:url_user + '?action=delete_healt_worker',
        data:{id : id},
        dataType:'json',
        beforeSend:function(){
      },
      success:function(response){
        // console.log(response);
        if (response.status == true) {
          swal("Success", response.message, "success");
          show_user();

        }else{
          console.log(response);
        }
      },
      error: function(error){
        console.log(error);
      }
      });
    });
  }

  function edit_user(_this){
    let id = $(_this).attr('data-id');
    $.ajax({
        type:"GET",
        url:url_user+'?action=find_user',
        data:{id : id},
        dataType:'json',
        beforeSend:function(){
        },
        success:function(response){
          // console.log(response);
         if (response.status == true) {
            $(".midwife-modal-title").text('Edit Health Worker');
            $('#user_id').val(response.data.user_id);
            $('#firstname').val(response.data.firstname);
            $('#lastname').val(response.data.lastname);
            $('#birthdate').val(response.data.birthdate);
            $('#gender').val(response.data.gender);
            $('#address').val(response.data.address);
            $('#username').val(response.data.username);
            $('#password').val(response.data.password);
            $('.password').addClass('hide');
            $('#user_type').val(response.data.user_type);
            $("#security_question1").val(response.data.security_question1);
            $("#security_answer1").val(response.data.security_answer1);
            $("#security_question2").val(response.data.security_question2);
            $("#security_answer2").val(response.data.security_answer2);
            $("#modal_add_midwife").modal({'backdrop' : 'static'});
         }else{
          console.log(response);
         }
        },
        error: function(error){
          console.log(error);
        }
      });

  }
</script>