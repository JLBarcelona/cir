<!DOCTYPE html>
<html>
  <title>Children - Deceased</title>
  <?php include("./Layout/header.php") ?>
  <!-- Header css meta -->
  
<body class="sidebar-mini layout-fixed" onload="active_tab('death_tab'); show_death();">
  <script src="../webroot/Signature/signature.js"></script>
  <div class="wrapper">
  <!-- navbar -->
  <?php include("./Layout/nav.php") ?>
  <!-- Sidebar -->
  <?php include("./Layout/sidebar.php") ?>
   <section class="content-wrapper">
      <div class="container-fluid">
        <div class="row pt-3">
          <div class="col-sm-12">  
            <div class="card">
              <div class="card-header bg-success">
                <span class="h4"><i class="fa fa-baby"></i> Deceased Children's Record</span>
                <button class="btn btn-sm btn-dark float-right" onclick="add_records();"><i class="fa fa-plus"></i></button>
              </div>
              <div class="card-body">
               
                <table class="table table-bordered dt-responsive nowrap" id="tbl_death" style="width: 100%;"></table>
              </div>
              <div class="card-footer"></div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
</body>

 <div class="modal fade" role="dialog" id="modal_add_child_deacese">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <div class="modal-title h4 child-modal-title">
            Add Deceased Child
            </div>
            <button class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
            <form class="needs-validation" id="death_form" action="#" novalidate>
              <div class="form-row">
                <input type="hidden" id="death_id" name="death_id" placeholder="" class="form-control" required>
                <label>Child Name </label>
                  <div class="form-group col-sm-12">
                  <select id="child_id" name="child_id" class="form-control child_id js-example-placeholder-single js-states"></select>
                  <div class="invalid-feedback" id="err_child_id" data-custom-validator="Child name is required!"></div>
                </div>
                  <div class="form-group col-sm-12">
                  <label>Cause Of Death </label>
                  <textarea type="text" rows="5" id="cause_of_death" name="cause_of_death" placeholder="Please put the child's cause of death" class="form-control" required></textarea>
                  <div class="invalid-feedback" id="err_cause_of_death"></div>
                </div>
                <div class="form-group col-sm-12">
                  <label>Death Date Time </label>
                  <input type="datetime-local" id="death_date_time" name="death_date_time" placeholder="" class="form-control " required>
                  <div class="invalid-feedback" id="err_death_date_time"></div>
                </div>

                 <div class="col-sm-12 form-group">
                       <label>Signature</label>
                       <textarea name="signature" id="signature" class="form-control hide"></textarea>
                        <div class="invalid-feedback" id="err_signature"></div>
                      <div class="border">
                        <canvas id="signature-pad" class="signature-pad" style="width: 100%; height:250px;" onmouseup="get_source();" ontouchend="get_source();"></canvas>
                      </div>
                    </div>
                  </div>

                <div class="col-sm-12 text-right">
                    <button class="btn btn-dark float-left mr-1" id="undo" type="button">Undo</button>
                  <button class="btn btn-dark float-left" id="clear" type="button">Clear</button>
                  <button class="btn btn-success" type="submit">Submit</button>
                </div>
              </div>
            </form>
          </div>
          <div class="modal-footer">
           
          </div>
        </div>
      </div>
    </div>
  <!-- Footer Scripts -->
  <?php include("./Layout/footer.php") ?>
</html>
<script>
  function add_records(){
    $("#modal_add_child_deacese").modal({'backdrop':'static'});
    $('#death_id').val('');
    $('#child_id').val('');
    $('.child_id').trigger('change');
    $('#cause_of_death').val('');
    $('#death_date_time').val('');
    $('#signature').val('');
    $("#clear").click();
    show_children_list();
    setTimeout(function(){
      resizeCanvas();
    },500);
  }

  var tbl_death;
  function show_death(){
    if (tbl_death) {
      tbl_death.destroy();
    }
    var url = url_user + '?action=show_child_death';
    tbl_death = $('#tbl_death').DataTable({
    pageLength: 10,
    responsive: true,
    ajax: url,
    deferRender: true,
    language: {
    "emptyTable": "No data available"
  },
    columns: [{
    className: '',
    "data": "child_id",
    "title": "Child Name",
    "render": function(data, type, row, meta){
        var param_data = JSON.stringify(row);
        return row.firstname+' '+row.lastname;
      }
  },{
    className: '',
    "data": "cause_of_death",
    "title": "Cause of death",
  },{
    className: '',
    "data": "death_date_time",
    "title": "Death Date & Time",
  },{
    className: 'width-1 text-center',
    "data": "death_id",
    "orderable": false,
    "title": "Options",
      "render": function(data, type, row, meta){
        var param_data = JSON.stringify(row);
        newdata = '';
        newdata += '<button class="btn btn-danger btn-sm font-base mt-1" data-id=\' '+row.death_id+'\' data-child=\' '+row.child_id+'\' onclick="delete_death(this)" type="button"><i class="fa fa-trash"></i> Delete</button>';
        return newdata;
      }
    }
  ]
  });
  }

  $("#death_form").on('submit', function(e){
    var url = url_user + '?action=add_child_death';
    var mydata = $(this).serialize();
    e.stopPropagation();
    e.preventDefault(e);
     swal({
        title: "Are you sure?",
        text: "Do you want to mark this child as deceased?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        closeOnConfirm: false
      },
      function(){
         $.ajax({
        type:"POST",
        url:url,
        data:mydata,
        cache:false,
        dataType:'json',
        beforeSend:function(){
            //<!-- your before success function -->
        },
        success:function(response){
            console.log(response)
          if(response.status == true){
            console.log(response)
            swal("Success", response.message, "success");
            showValidator(response.error,'death_form');
            show_death();
            show_children_list();
            $("#modal_add_child_deacese").modal('hide');
          }else{
            //<!-- your error message or action here! -->
            swal.close();
            showValidator(response.error,'death_form');
          }
        },
        error:function(error){
          console.log(error)
        }
      });
    });

    
  });

  function delete_death(_this){
    var id = JSON.parse($(_this).attr('data-id'));
    var child = JSON.parse($(_this).attr('data-child'));
    var url = url_user + '?action=delete_child_death';

      swal({
        title: "Are you sure?",
        text: "Do you want to delete this death?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        closeOnConfirm: false
      },
      function(){
        $.ajax({
        type:"POST",
        url:url,
        data:{id:id, child:child},
        dataType:'json',
        beforeSend:function(){
      },
      success:function(response){
        // console.log(response);
        if (response.status == true) {
          show_death();
          swal("Success", response.message, "success");
        }else{
          console.log(response);
        }
      },
      error: function(error){
        console.log(error);
      }
      });
    });
  }

  function edit_death(_this){
    var data = JSON.parse($(_this).attr('data-info'));
    $('#death_id').val(data.death_id);
    $('#child_id').val(data.child_id);
    $('#cause_of_death').val(data.cause_of_death);
    $('#death_date_time').val(data.death_date_time);
    // $('#signature').val(data.death_date_time);
  }


   show_children_list();
    $('.child_id').select2({
       placeholder: {
        id: '-1', // the value of the option
        text: 'Select Child name'
      },
      theme: 'bootstrap4',
      allowClear: true
    });
</script>
<script src="../webroot/js/signatures.js"></script>
