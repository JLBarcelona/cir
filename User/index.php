<!DOCTYPE html>
<html>
<title>Dashboard</title>
  <?php include("./Layout/header.php") ?>
  <script src="../webroot/highcharts/highcharts.js"></script>
  <script src="../webroot/highcharts/data.js"></script>
  <script src="../webroot/highcharts/drilldown.js"></script>
  <script src="../webroot/highcharts/exporting.js"></script>
  <script src="../webroot/highcharts/export-data.js"></script>
  <script src="../webroot/highcharts/accessibility.js"></script>
  <!-- Header css meta -->
<body class="sidebar-mini layout-fixed" onload="active_tab('dashboard_tab');">
  <div class="wrapper">
  <!-- navbar -->
  <?php include("./Layout/nav.php") ?>
  <!-- Sidebar -->
  <?php include("./Layout/sidebar.php") ?>
   <section class="content-wrapper">
      <div class="container-fluid">
        <div class="row pt-3">
          <div class="col-sm-12">
              <div class="card">
              <div class="card-header bg-success">
                <div class="row">
                  <div class="col-sm-4 col-6">
                    <span class="h4"><i class="fa fa-tachometer-alt"></i> Dashboard</span>
                  </div>
                  <div class="col-sm-8 col-12">
                    <div class="row">
                      <div class="col-sm-6"></div>
                      <div class="col-sm-6 col-9 text-right">
                        <select class="form-control " style="width:60%; display: inline-block; height: 33.5px" id="purok">
                          <option value="all">All</option>  
                          <option value="1">Purok 1</option>
                          <option value="2">Purok 2</option>
                          <option value="3">Purok 3</option>
                          <option value="4">Purok 4</option>
                          <option value="5">Purok 5</option>
                        </select>
                        <button class="btn btn-dark" onclick="get_stats();"><i class="fa fa-search"></i></button>
                        <button class="btn btn-danger"><i class="fa fa-times"></i></button>
                      </div>
                    </div>
                  </div>
                </div>                
              </div>
              <div class="card-body">
                 <figure class="highcharts-figure">
                    <div id="container"></div>
                </figure>
              </div>
              <div class="card-footer"></div>
            </div>
            
          </div>
        </div>
      </div>
    </section>
  </div>
</body>

<div class="modal fade" role="dialog" id="modal_details">
    <div class="modal-dialog modal-lg" style="max-width: 80% !important;">
      <div class="modal-content">
        <div class="modal-header">
          <div class="modal-title">
          Details
          </div>
          <button class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <table class="table table-bordered dt-responsive nowrap" id="tbl_user" style="width: 100%;"></table>
        </div>
        <div class="modal-footer">
          
        </div>
      </div>
    </div>
  </div>

  <?php include("./Layout/footer.php") ?>

  <script type="text/javascript">
    var tbl_user;

    function show_details(data){
      show_user(data);
      $("#modal_details").modal('show');
    }

    function show_user(data){
      if (tbl_user) {
        tbl_user.destroy();
      }
      var url = main_path + '/user/list_user';
      tbl_user = $('#tbl_user').DataTable({
      pageLength: 10,
      responsive: true,
      data: data,
      deferRender: true,
      language: {
      "emptyTable": "No data available"
    },
      columns: [{
      className: '',
      "data": "firstname",
      "title": "Name",
      "render":function(data, type, row, meta){
        return newdata = '<a target="_blank" href="print_multiple.php?r=d210658dbb82bd59bd7ea38ed51eb283&appid='+row.child_id+'" class="btn btn-link btn-sm font-base mt-1">'+row.lastname+', '+row.firstname+'</a>';
        // return row.lastname+', '+row.firstname;
      }
    },{
      className: '',
      "data": "kind_of_birth",
      "title": "Kind of Birth"
    },{
      className: '',
      "data": "father_name",
      "title": "Father's name"
    },{
      className: '',
      "data": "mother_name",
      "title": "Mother's name"
    },{
      className: '',
      "data": "mother_name",
      "title": "Address",
      "render":function(data, type, row, meta){
        return row.barangay+' purok '+row.purok+', '+row.street+' '+row.address;
      }
    }
    ]
    });
    }
  </script>

<script>
get_stats();
function get_stats(){
  let param = $("#purok").val();
  let url = url_user+'?action=get_stats_data';
  $.ajax({
    type:"GET",
    url:url,
    data:{param:param},
    dataType: 'json',
    beforeSend:function(){

    },
    success:function(response){
      // console.log(response);
      draw_chart(response.data, response.date);
    }
  });
}

  // Create the chart
function draw_chart(data, date){
  Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: date
    },
    subtitle: {
        text: 'Click the columns to view versions. Source: <a href="http://statcounter.com" target="_blank">statcounter.com</a>'
    },
    accessibility: {
        announceNewData: {
            enabled: true
        }
    },
    xAxis: {
        type: 'category'
    },
    yAxis: {
        title: {
            text: 'Total percent market share'
        }

    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y:.1f}%'
            }, 
        events: {
              click: function(e) {
                  // console.log(e.point.datas);
                  show_details(e.point.datas);
              }
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
    },

    series: [
        {
            name: "Children's Statistical Information",
            colorByPoint: true,
             data: JSON.parse(data)
        }
    ],
    drilldown: {
       
    }
});
}
</script>
