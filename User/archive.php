<!DOCTYPE html>
<html>
  <title>Archive</title>
  <?php include("./Layout/header.php") ?>
  <!-- Header css meta -->
<body class="sidebar-mini layout-fixed" onload="active_tab('archive_tab');">
  <div class="wrapper">
  <!-- navbar -->
  <?php include("./Layout/nav.php") ?>
  <!-- Sidebar -->
  <?php include("./Layout/sidebar.php") ?>
   <section class="content-wrapper">
      <div class="container-fluid">
        <div class="row pt-3">
          <div class="col-sm-12">  
            <div class="card">
              <div class="card-header bg-success">
                <span class="h4"><i class="fa fa-archive"></i> Archives</span>
              </div>
              <div class="card-body">
                 <!--  <div class="card">
                    <div class="card-header bold">Graduated Children </div>
                    <div class="card-body">
                      <table class="table table-bordered dt-responsive nowrap" id="tbl_children_graduated" style="width: 100%;"></table>
                    </div>
                    <div class="card-footer"></div>
                  </div> -->

                 <div class="card">
                  <div class="card-header bold">Health Worker </div>
                  <div class="card-body">
                    <table class="table table-bordered dt-responsive nowrap" id="tbl_user" style="width: 100%;"></table>
                  </div>
                  <div class="card-footer"></div>
                </div>

                <div class="card">
                  <div class="card-header bold">Midwife </div>
                  <div class="card-body">
                    <table class="table table-bordered dt-responsive nowrap" id="tbl_midwife" style="width: 100%;"></table>
                  </div>
                  <div class="card-footer"></div>
                </div>

                 <div class="card">
                  <div class="card-header bold">Children </div>
                  <div class="card-body">
                    <table class="table table-bordered dt-responsive nowrap" id="tbl_children" style="width: 100%;"></table>
                  </div>
                  <div class="card-footer"></div>
                </div>

              </div>
              <div class="card-footer"></div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
</body>
  <!-- Footer Scripts -->
  <?php include("./Layout/footer.php") ?>
</html>
<script type="text/javascript">
   function recover_children(_this){
    var id =  $(_this).attr('data-id');
    var url = url_user+'?action=recover_child';
    swal({
          title: "Are you sure?",
          text: "Do you want to recover this record?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes",
          closeOnConfirm: false
        },
          function(){
           $.ajax({
            type:"POST",
            url:url,
            data:{id:id},
            dataType:'json',
            beforeSend:function(){
            },
            success:function(response){
              // console.log(response);
             if (response.status == true) {
                swal("Success", response.message, "success");
                show_children();
             }else{
              console.log(response);
             }
            },
            error: function(error){
              console.log(error);
            }
          });
        });
  }

  function recover_users(_this){
    var id = $(_this).attr('data-id');
    var url = url_user+'?action=recover_user';
    swal({
          title: "Are you sure?",
          text: "Do you want to recover this record?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes",
          closeOnConfirm: false
        },
          function(){
           $.ajax({
            type:"POST",
            url:url,
            data:{id:id},
            dataType:'json',
            beforeSend:function(){
            },
            success:function(response){
              // console.log(response);
             if (response.status == true) {
                swal("Success", response.message, "success");
                  show_midwife();
                  show_user();
             }else{
              console.log(response);
             }
            },
            error: function(error){
              console.log(error);
            }
          });
        });
  }
</script>
<!-- Javascript Function-->
<script>
  show_midwife();
  show_user();
   var tbl_user;
  function show_user(){
    if (tbl_user) {
      tbl_user.destroy();
    }
    tbl_user = $('#tbl_user').DataTable({
    pageLength: 10,
    responsive: true,
    ajax: url_user+'?action=health_worker_list_archive',
    deferRender: true,
    language: {
    "emptyTable": "No data available"
  },
    columns: [{
    className: '',
    "data": "lastname",
    "title": "Name",
    "render": function(data, type, row, meta){
      return row.lastname+', '+row.firstname;
    }
  },{
    className: '',
    "data": "lastname",
    "title": "Lastname",
  },{
    className: '',
    "data": "birthdate",
    "title": "Birthdate",
  },{
    className: '',
    "data": "gender",
    "title": "Gender",
  },{
    className: '',
    "data": "address",
    "title": "Address",
  },{
    className: '',
    "data": "username",
    "title": "Username",
  },
  {
    className: 'width-1 text-center',
    "data": "",
    "title": "Action",
    "render": function(data, type, row, meta){
      return '<button class="btn btn-dark btn-sm" data-id=\''+row.user_id+'\' onclick="recover_users(this)">Recover</button>';
    }
  }
  ]
  });
  }


  var tbl_midwife;
  function show_midwife(){
    if (tbl_midwife) {
      tbl_midwife.destroy();
    }
    tbl_midwife = $('#tbl_midwife').DataTable({
    pageLength: 10,
    responsive: true,
    ajax: url_user+'?action=midwife_list_archive',
    deferRender: true,
    language: {
    "emptyTable": "No data available"
  },
    columns: [{
    className: '',
    "data": "lastname",
    "title": "Name",
    "render": function(data, type, row, meta){
      return row.lastname+', '+row.firstname;
    }
  },{
    className: '',
    "data": "lastname",
    "title": "Lastname",
  },{
    className: '',
    "data": "birthdate",
    "title": "Birthdate",
  },{
    className: '',
    "data": "gender",
    "title": "Gender",
  },{
    className: '',
    "data": "address",
    "title": "Address",
  },{
    className: '',
    "data": "username",
    "title": "Username",
  },
  {
    className: 'width-1 text-center',
    "data": "",
    "title": "Action",
    "render": function(data, type, row, meta){
      return '<button class="btn btn-dark btn-sm" data-id=\''+row.user_id+'\' onclick="recover_users(this)">Recover</button>';
    }
  }
  ]
  });
  }

  show_children();
  var tbl_children;
  function show_children(){
    if (tbl_children) {
      tbl_children.destroy();
    }
    tbl_children = $('#tbl_children').DataTable({
    pageLength: 10,
    responsive: true,
    ajax: url_user+'?action=children_list_archive',
    deferRender: true,
    language: {
    "emptyTable": "No data available"
  },
    columns: [{
    className: '',
    "data": "lastname",
    "title": "Name",
    "render": function(data, type, row, meta){
      let middlename = (row.middlename !== null && row.middlename !== '') ? row.middlename : '';
      return row.lastname+', '+row.firstname+' '+middlename;
    }
  },{
    className: '',
    "data": "birthdate",
    "title": "Birthdate",
  },{
    className: '',
    "data": "gender",
    "title": "Gender",
  },{
    className: '',
    "data": "barangay",
    "title": "Address",
    "render": function(data, type, row, meta){
      return 'Purok#'+row.purok+', '+row.street+", "+row.barangay+", "+row.address;
    }
  },{
    className: '',
    "data": "kind_of_birth",
    "title": "Kind of birth",
  },{
    className: '',
    "data": "mother_name",
    "title": "Mother's name",
  },{
    className: '',
    "data": "father_name",
    "title": "Father's name",
  },{
    className: '',
    "data": "contact_number",
    "title": "CP#",
  },
  {
    className: 'width-1 text-center',
    "data": "",
    "title": "Action",
    "render": function(data, type, row, meta){
      return '<button class="btn btn-dark btn-sm" data-id=\''+row.child_id+'\' onclick="recover_children(this)">Recover</button>';
    }
  }
  ]
  });
  }

 


  show_children_graduated();
  var tbl_children_graduated;
  function show_children_graduated(){
    if (tbl_children_graduated) {
      tbl_children_graduated.destroy();
    }
    tbl_children_graduated = $('#tbl_children_graduated').DataTable({
    pageLength: 10,
    responsive: true,
    ajax: url_user+'?action=children_list_archive_graduate',
    deferRender: true,
    language: {
    "emptyTable": "No data available"
  },
    columns: [{
    className: '',
    "data": "lastname",
    "title": "Name",
    "render": function(data, type, row, meta){
      let middlename = (row.middlename !== null && row.middlename !== '') ? row.middlename : '';
      return row.lastname+', '+row.firstname+' '+middlename;
    }
  },{
    className: '',
    "data": "birthdate",
    "title": "Birthdate",
  },{
    className: '',
    "data": "gender",
    "title": "Gender",
  },{
    className: '',
    "data": "barangay",
    "title": "Address",
    "render": function(data, type, row, meta){
      return 'Purok#'+row.purok+', '+row.street+", "+row.barangay+", "+row.address;
    }
  },{
    className: '',
    "data": "kind_of_birth",
    "title": "Kind of birth",
  },{
    className: '',
    "data": "mother_name",
    "title": "Mother's name",
  },{
    className: '',
    "data": "father_name",
    "title": "Father's name",
  },{
    className: '',
    "data": "contact_number",
    "title": "CP#",
  },
  {
    className: 'width-1 text-center',
    "data": "",
    "title": "Action",
    "render": function(data, type, row, meta){
      return '<button class="btn btn-dark btn-sm" data-id=\''+row.child_id+'\' onclick="recover_children(this)">Recover</button>';
    }
  }
  ]
  });
  }

 
</script>
 