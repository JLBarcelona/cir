<div class="modal fade" role="dialog" id="add_sched_modal">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <div class="modal-title">
          Add Schedule
          </div>
          <button class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <form class="needs-validation" id="sched_form" action="#" novalidate>
            <div class="form-row">
              <input type="hidden" id="appointment_id" name="appointment_id" placeholder="" class="form-control" required>
              
                <div class="form-group col-sm-12">
                  <label>Children </label>
                  <select id="child_id" name="child_id[]" class="form-control child_id" multiple="multiple" data-placeholder="Add children"></select>
                  <div class="invalid-feedback" id="err_child_id" data-custom-validator="Child name is required!"></div>
                </div>

                <div class="form-group col-sm-12">
                  <label>Vaccine </label>
                  <div class="input-group mb-3">
                   <select id="vac_id" name="vac_id[]" class="form-control vac_id" multiple="multiple" data-placeholder="Add Vaccine"></select>
                    <div class="input-group-append">
                      <button class="btn btn-outline-secondary" type="button" onclick="add_vaccine();"><i class="fa fa-plus"></i></button>
                    </div>
                    <div class="invalid-feedback" id="err_vac_id" data-custom-validator="Vaccine is required!"></div>
                  </div>
                </div>

                <div class="form-group col-sm-6">
                  <label>Appointment Date </label>
                  <input type="date" id="appointment_date" name="appointment_date" placeholder="" class="form-control " required>
                  <div class="invalid-feedback" id="err_appointment_date" ></div>
                </div>
                <div class="form-group col-sm-6">
                  <label>Time </label>
                  <input type="time" id="appointment_time" name="appointment_time" placeholder="" class="form-control " required>
                  <div class="invalid-feedback" id="err_appointment_time" ></div>
                </div>

              <div class="col-sm-12 text-right">
                <button class="btn btn-success" type="submit">Submit</button>
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          
        </div>
      </div>
    </div>
  </div>

  <?php include("./Vaccine/vaccine_add.php") ?>
  <script type="text/javascript">
    function finish_all_check(){
      let data = $("#form_table_sched").serialize();
      var url = url_user+'?action=finish_all_check';
      // console.log(data);
        swal({
              title: "Are you sure?",
              text: "Do you want to mark this schedule as finished?",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Yes",
              closeOnConfirm: false
            },
              function(){
               $.ajax({
                type:"POST",
                url:url,
                data:data,
                dataType:'json',
                beforeSend:function(){
                },
                success:function(response){
                  // console.log(response);
                 if (response.status == true) {
                    swal("Success", response.message, "success");
                   
                    $(".actions").click();
                    $(".all_action").attr('disabled', true);
                    show_calendar();
                    show_on_sched(data_table_record);
                    
                 }else{
                  console.log(response);
                 }
                },
                error: function(error){
                  console.log(error);
                }
              });
            });
      }


      function delete_all_check(){
      let data = $("#form_table_sched").serialize();
      var url = url_user+'?action=delete_all_check';
      // console.log(data);
        swal({
              title: "Are you sure?",
              text: "Do you want to delete ?",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Yes",
              closeOnConfirm: false
            },
              function(){
               $.ajax({
                type:"POST",
                url:url,
                data:data,
                dataType:'json',
                beforeSend:function(){
                },
                success:function(response){
                  // console.log(response);
                 if (response.status == true) {
                    swal("Success", response.message, "success");
                    $(".actions").click(); 
                    $(".all_action").attr('disabled', true);
                    show_calendar();
                    show_on_sched(data_table_record); 
                 }else{
                  console.log(response);
                 }
                },
                error: function(error){
                  console.log(error);
                }
              });
            });
      }

    function check_uncheck_checkbox(isChecked) {
      if(isChecked) {
        $('input[name="selected_child[]"]').each(function() { 
          this.checked = true; 
          $(".all_action").attr('disabled', false);
        });
      } else {
        $('input[name="selected_child[]"]').each(function() {
          this.checked = false;
          $(".all_action").attr('disabled', true);
        });
      }
    }


     function delete_appointment(id){
        swal({
          title: "Are you sure?",
          text: "Do you want to delete this schedule?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes",
          closeOnConfirm: false
        },
        function(){
          $.ajax({
          type:"POST",
          url:url_user+'?action=delete_schedule',
          data:{id:id},
          dataType:'json',
          beforeSend:function(){
        },
        success:function(response){
          // console.log(response);
          if (response.status == true) {
            // $("#modal_sched").modal('hide');
            
            // console.log(data_table_record);
            swal("Success", response.message, "success");
            show_calendar();
            show_on_sched(data_table_record);
             $(".actions").click();
             $(".all_action").attr('disabled', true);
          }else{
            console.log(response);
          }
        },
        error: function(error){
          console.log(error);
        }
        });
      });
    }

      function mark_finish_appointment(id){
        swal({
          title: "Are you sure?",
          text: "Do you want to mark this schedule as finished?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes",
          closeOnConfirm: false
        },
        function(){
          $.ajax({
          type:"POST",
          url:url_user+'?action=mark_finish_schedule',
          data:{id:id},
          dataType:'json',
          beforeSend:function(){
        },
        success:function(response){
          // console.log(response);
          if (response.status == true) {
          
            
            // $("#modal_sched").modal('hide');
            swal("Success", response.message, "success");
            show_calendar();
            show_on_sched(data_table_record);
             $(".actions").click();
             $(".all_action").attr('disabled', true);
          }else{
            console.log(response);
          }
        },
        error: function(error){
          console.log(error);
        }
        });
      });
    }


    function add_sched(date_val){
        if (date_val !== '') {
          $("#appointment_date").attr('readonly', true);
        }else{
          $("#appointment_date").attr('readonly', false);
        }
        $("#add_sched_modal").modal({'backdrop' : 'static'});
        $("#appointment_date").val(date_val);
        $("#child_id").val('');
        $("#description").val('');
        $('.child_id').trigger('change');
        $('#vac_id').val('');
        $('.vac_id').trigger('change');
        $("#appointment_time").val('');
        showValidator('','sched_form');

      }


      $("#sched_form").on('submit', function(e){
          var url = $(this).attr('action');
          var mydata = $(this).serialize();
          e.stopPropagation();
          e.preventDefault(e);

          let child_id = $("#child_id").val();
          let add_url_child = (child_id == '')? '&child_id=' : '';

          let vac_id = $("#vac_id").val();
          let add_url_vac = (vac_id == '')? '&vac_id=' : '';

          $.ajax({
            type:"POST",
            url:url_user+'?action=save_sched',
            data:mydata+add_url_child+add_url_vac,
            cache:false,
            dataType: 'json',
            beforeSend:function(){
                //<!-- your before success function -->
            },
            success:function(response){
                // console.log(mydata);
                console.log(response);
              if(response.status == true){
                // console.log(response);
                $("#add_sched_modal").modal('hide');

                swal("Success", response.message, "success");
                showValidator(response.error,'sched_form');
                show_calendar();
              }else{
                //<!-- your error message or action here! -->
                showValidator(response.error,'sched_form');
              }
            },
            error:function(error){
              console.log(error)
            }
          });
        });

         show_children_list();
        $('.child_id').select2({
          theme: 'bootstrap4',
        });

        show_vaccines();
        $('.vac_id').select2({
          theme: 'bootstrap4',
        });

  </script>