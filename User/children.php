<!DOCTYPE html>
<html>
  <title>Children</title>
  <?php include("./Layout/header.php") ?>
  <!-- Header css meta -->
<body class="sidebar-mini layout-fixed" onload="active_tab('children_tab'); show_children();">
  <div class="wrapper">
  <!-- navbar -->
  <?php include("./Layout/nav.php") ?>
  <!-- Sidebar -->
  <?php include("./Layout/sidebar.php") ?>
   <section class="content-wrapper">
      <div class="container-fluid">
        <div class="row pt-3">
          <div class="col-sm-12">  
            <div class="card">
              <div class="card-header bg-success">
                <span class="h4"><i class="fa fa-baby"></i> Children's Profile</span>
                   <?php if (Auth::user('user_type') == 3 ): ?>
                    <?php else: ?>
                      <button class="btn btn-sm btn-dark float-right" onclick="add_children();"><i class="fa fa-plus"></i></button>                      
                   <?php endif ?>
              </div>
              <div class="card-body">
                <table class="table table-bordered dt-responsive nowrap" id="tbl_children" style="width: 100%;"></table>
              </div>
              <div class="card-footer"></div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
</body>

  <div class="modal fade" role="dialog" id="milestone_modal">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <div class="modal-title h4 child-timeline-modal-title">
          
          </div>
          <button class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
            <section class="content">
              <div class="container-fluid">

                <!-- Timelime example  -->
                <div class="row">
                  <div class="col-md-12">
                    <!-- The time line -->
                    <div class="timeline" id="content_timeline">
                      <!-- END timeline item -->
                    </div>
                  </div>
                  <!-- /.col -->
                </div>
              </div>
              <!-- /.timeline -->
            </section>
        </div>
        <div class="modal-footer">
        </div>
      </div>
    </div>
  </div>

 <div class="modal fade" role="dialog" id="modal_add_child">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <div class="modal-title h4 child-modal-title">
            Add Children
            </div>
            <button class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
            <form class="needs-validation" id="sample_form_id" action="#" novalidate>
              <div class="form-row">
                <input type="hidden" id="child_id" name="child_id" placeholder="" class="form-control" required>
                <div class="form-group col-sm-12">
                  <label>Firstname </label>
                  <input type="text" id="firstname" name="firstname" placeholder="Firstname" class="form-control " required>
                  <div class="invalid-feedback" id="err_firstname"></div>
                </div>
                <div class="form-group col-sm-12">
                  <label>Middlename <small>(optional)</small></label>
                  <input type="text" id="middlename" name="middlename" placeholder="Middlename (Optional)" class="form-control " required>
                  <div class="invalid-feedback" id="err_middlename"></div>
                </div>
                <div class="form-group col-sm-12">
                  <label>Lastname </label>
                  <input type="text" id="lastname" name="lastname" placeholder="Lastname" class="form-control " required>
                  <div class="invalid-feedback" id="err_lastname"></div>
                </div>
                <!-- <div class="form-group col-sm-12">
                  <label>Nickname </label>
                  <input type="text" id="nickname" name="nickname" placeholder="" class="form-control " required>
                  <div class="invalid-feedback" id="err_nickname"></div>
                </div> -->
                <div class="form-group col-sm-6">
                  <label>Birthdate </label>
                  <input type="date" id="birthdate" name="birthdate" placeholder="Birthdate" class="form-control " required>
                  <div class="invalid-feedback" id="err_birthdate"></div>
                </div>

                <div class="form-group col-sm-6">
                    <label>Gender </label>
                    <select id="gender" name="gender" class="form-control ">
                      <option value="" selected="">Select Gender</option>
                      <option value="Male">Male</option>
                      <option value="Female">Female</option>
                    </select>
                    <div class="invalid-feedback" id="err_gender"></div>
                  </div>
<!--                 <div class="form-group col-sm-4">
                  <label>Barangay </label>
                  <input type="text" id="barangay" name="barangay" placeholder="" class="form-control " required>
                  <div class="invalid-feedback" id="err_barangay"></div>
                </div> -->
                <div class="form-group col-sm-6">
                  <label>Purok </label>
                  <input type="text" id="purok" name="purok" placeholder="Purok" class="form-control " required>
                  <div class="invalid-feedback" id="err_purok"></div>
                </div>
                <div class="form-group col-sm-6">
                  <label>Street </label>
                  <input type="text" id="street" name="street" placeholder="Street" class="form-control " required>
                  <div class="invalid-feedback" id="err_street"></div>
                </div>
<!--                 <div class="form-group col-sm-12">
                  <label>Address </label>
                  <input type="text" id="address" name="address" placeholder="" class="form-control " required>
                  <div class="invalid-feedback" id="err_address"></div>
                </div> -->
                <div class="form-group col-sm-12">
                  <label>Kind Of Birth </label>
                  <input type="text" id="kind_of_birth" name="kind_of_birth" placeholder="Kind of Birth" class="form-control " required>
                  <div class="invalid-feedback" id="err_kind_of_birth"></div>
                </div>
                <div class="form-group col-sm-12">
                  <label>Mother's Full Name </label>
                  <input type="text" id="mother_name" name="mother_name" placeholder="Mother's full name" class="form-control " required>
                  <div class="invalid-feedback" id="err_mother_name"></div>
                </div>
                <div class="form-group col-sm-12">
                  <label>Father's Full Name </label>
                  <input type="text" id="father_name" name="father_name" placeholder="Father's full name" class="form-control " required>
                  <div class="invalid-feedback" id="err_father_name"></div>
                </div>
                <div class="form-group col-sm-12">
                  <label>Contact Number </label>
                  <input type="text" id="contact_number" onkeypress="return num_only(event);" name="contact_number" placeholder="Contact Number" class="form-control " required>
                  <div class="invalid-feedback" id="err_contact_number"></div>
                </div>

                <div class="col-sm-12 text-right">
                  <button class="btn btn-success" type="submit">Submit</button>
                </div>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            
          </div>
        </div>
      </div>
    </div>
  <!-- Footer Scripts -->
  <?php include("./Layout/footer.php") ?>
</html>

<!-- Javascript Function-->
<script>
  function show_milestone(id, name){
    let this_ = $("#content_timeline");

    this_.html('');
      $.ajax({
        type:"POST",
        url:url_user+'?action=show_child_milestone',
        data:{child_id:id},
        dataType:'json',
        beforeSend:function(){
      },
      success:function(response){
        // console.log(response);
        
        let res = response.data;

        let footer = ` <div>
                        <i class="fas fa-clock bg-gray"></i>
                      </div>`;

        let output = res.map(row => {
          let out = '';
          let des = (row.description == '' || row.description == null)? 'No description' : row.description;
          out += `<div class="time-label">
            <span class="bg-primary">`+row.age+`</span>
            </div>
            <div>
            <i class="fa fa-child bg-purple"></i>
            <div class="timeline-item">
            <div class="timeline-body">
            <p>`+des+`</p>`;
            if (row.milestone == null || row.milestone == '') {
              out += '';
            }else{
              out += `<div><img src="`+row.milestone+`" class=" img-fluid"></div>`;

            }            
            out+=`</div>
            </div>
            </div>`;

            return out;
        });

        this_.append(output);
        // if (response.status == true) {
        //   swal("Success", response.message, "success");
        //   show_children();
        // }else{
        //   console.log(response);
        // }
      },
      error: function(error){
        console.log(error);
      }
      });

    $(".child-timeline-modal-title").text(name+'\'s Milestone');
    $("#milestone_modal").modal({'backdrop' : 'static'});
  }

  function add_children(){
    $('#child_id').val('');
    $('#firstname').val('');
    $('#lastname').val('');
    $("#middlename").val('');
    // $('#nickname').val('');
    $('#birthdate').val('');
    // $('#barangay').val('');
    $("#gender").val('');
    $('#purok').val('');
    $('#street').val('');
    // $('#address').val('');
    $('#kind_of_birth').val('');
    $('#mother_name').val('');
    $('#father_name').val('');
    $('#contact_number').val('');
    $("#modal_add_child").modal({'backdrop' : 'static'});
    $(".child-modal-title").text('Add Children');
  }

  var tbl_children;
  function show_children(){
    if (tbl_children) {
      tbl_children.destroy();
    }
    tbl_children = $('#tbl_children').DataTable({
    pageLength: 10,
    responsive: true,
    ajax: url_user+'?action=children_list',
    deferRender: true,
    language: {
    "emptyTable": "No data available"
  },
    columns: [{
    className: '',
    "data": "lastname",
    "title": "Name",
    "render": function(data, type, row, meta){
      let middlename = (row.middlename !== null && row.middlename !== '') ? row.middlename : '';
      return row.lastname+', '+row.firstname+' '+middlename;
    }
  },{
    className: '',
    "data": "birthdate",
    "title": "Birthdate",
  },{
    className: '',
    "data": "gender",
    "title": "Gender",
  },{
    className: '',
    "data": "barangay",
    "title": "Address",
    "render": function(data, type, row, meta){
      return 'Purok#'+row.purok+', '+row.street+", "+row.barangay+", "+row.address;
    }
  },{
    className: '',
    "data": "kind_of_birth",
    "title": "Kind of birth",
  },{
    className: '',
    "data": "mother_name",
    "title": "Mother's name",
  },{
    className: '',
    "data": "father_name",
    "title": "Father's name",
  },{
    className: '',
    "data": "contact_number",
    "title": "CP#",
  },{
    className: '',
    "data": "archive_at",
    "title": "Status",
    "render":function(data, type, row, meta){
      if (row.archive_at == '' || row.archive_at === null) {
        return 'On Going';
      }else{
        return 'Graduate';
      }
    }
  },{
    className: 'width-option-1 text-center',
    "data": "children_id",
    "orderable": false,
    "title": "Options",
      "render": function(data, type, row, meta){
        var param_data = JSON.stringify(row);
        newdata = '';
        newdata += '<button class="btn btn-success btn-sm font-base mt-1" data-info=\' '+param_data.trim()+'\' onclick="edit_user(this)" type="button"><i class="fa fa-edit"></i> Edit</button> ';
        newdata += '<button class="btn btn-info btn-sm font-base mt-1" onclick="show_milestone(\''+row.child_id+'\',\''+row.firstname+'\')" type="button"><i class="fa fa-history"></i> Milestone</button> ';
        newdata += `<span class="dropdown">
              <button class="btn btn-dark btn-sm font-base mt-1 dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-cog"></i> Others
              </button>
              <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="javascript:;" data-type="3" data-info=\' `+param_data.trim()+`\' onclick="delete_user(this)">Mark as Graduate</a>
                <a class="dropdown-item" href="#" data-type="0" data-info=\' `+param_data.trim()+`\' onclick="delete_user(this)">Go to Archive</a>
                 <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="javascript:;" data-type="1" data-info=\' `+param_data.trim()+`\' onclick="delete_user(this)">Delete Permanently</a>
              </div>
            </span>`;
        newdata += '<a target="_blank" href="print_multiple.php?r=d210658dbb82bd59bd7ea38ed51eb283&appid='+row.child_id+'" class="btn btn-primary btn-sm font-base mt-1"><i class="fa fa-print"></i> Print</a>';
        return newdata;
      }
    }
  ]
  });
  }

  $("#sample_form_id").on('submit', function(e){
    var mydata = $(this).serialize();
    e.stopPropagation();
    e.preventDefault(e);

    $.ajax({
      type:"POST",
      url:url_user+'?action=add_child',
      data:mydata,
      cache:false,
      dataType:'json',
      beforeSend:function(){
          //<!-- your before success function -->
      },
      success:function(response){
          console.log(response);
        if(response.status == true){
          console.log(response);
          show_children();
          swal("Success", response.message, "success");
          showValidator(response.error,'sample_form_id');
          $("#modal_add_child").modal('hide');

        }else{
          //<!-- your error message or action here! -->
          showValidator(response.error,'sample_form_id');
        }
      },
      error:function(error){
        console.log(error)
      }
    });
  });

  function delete_user(_this){
    var type_delete = $(_this).data('type');
    var message = (type_delete == '1')? 'delete this child permanently?' : 'move this child record to archive?';
    message  = (type_delete == '3')? 'mark this child as graduate ?' : message;

    var data = JSON.parse($(_this).attr('data-info'));
      swal({
        title: "Are you sure?",
        text: "Do you want to "+message,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        closeOnConfirm: false
      },
      function(){
        $.ajax({
        type:"POST",
        url:url_user+'?action=delete_child',
        data:{child_id:data.child_id, type_delete : type_delete},
        dataType:'json',
        beforeSend:function(){
      },
      success:function(response){
        // console.log(response);
        if (response.status == true) {
          swal("Success", response.message, "success");
          show_children();
        }else{
          console.log(response);
        }
      },
      error: function(error){
        console.log(error);
      }
      });
    });
  }

  function edit_user(_this){
    var data = JSON.parse($(_this).attr('data-info'));
    $('#child_id').val(data.child_id);
    $('#firstname').val(data.firstname);
    $('#lastname').val(data.lastname);
    $("#middlename").val(data.middlename);
    // $('#nickname').val(data.nickname);
    $('#birthdate').val(data.birthdate);
    // $('#barangay').val(data.barangay);
    $("#gender").val(data.gender);
    $('#purok').val(data.purok);
    $('#street').val(data.street);
    // $('#address').val(data.address);
    $('#kind_of_birth').val(data.kind_of_birth);
    $('#mother_name').val(data.mother_name);
    $('#father_name').val(data.father_name);
    $('#contact_number').val(data.contact_number);
    $("#modal_add_child").modal({'backdrop' : 'static'});
    $(".child-modal-title").text('Edit Children');

  }
</script>