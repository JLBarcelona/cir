<!DOCTYPE html>
<html>
  <title>Logs</title>
  <?php include("./Layout/header.php") ?>
  <!-- Header css meta -->
<body class="sidebar-mini layout-fixed" onload="active_tab('logs_tab'); show_logs();">
  <div class="wrapper">
  <!-- navbar -->
  <?php include("./Layout/nav.php") ?>
  <!-- Sidebar -->
  <?php include("./Layout/sidebar.php") ?>
   <section class="content-wrapper">
      <div class="container-fluid">
        <div class="row pt-3">
          <div class="col-sm-12">  
            <div class="card">
              <div class="card-header bg-success">
                <span class="h4"><i class="fa fa-list"></i> Logs</span>
              </div>
              <div class="card-body">
                <table class="table table-bordered dt-responsive nowrap" id="tbl_logs" style="width: 100%;"></table>
              </div>
              <div class="card-footer"></div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
</body>
  <!-- Footer Scripts -->
  <?php include("./Layout/footer.php") ?>
</html>

<!-- Javascript Function-->
<script>
	  var tbl_logs;
  function show_logs(){
    if (tbl_logs) {
      tbl_logs.destroy();
    }
    var url = url_user + '?action=log_list';
    tbl_logs = $('#tbl_logs').DataTable({
    pageLength: 10,
    responsive: true,
    ajax: url,
    deferRender: true,
    language: {
    "emptyTable": "No data available"
  },
    columns: [{
    className: 'width-10',
    "data": "message",
    "title": "Message",
     "render": function(data, type, row, meta){
     	let me = '<?php echo Auth::user('user_id') ?>';
        
     	if (me == row.user_id) {
        	return 'You '+row.message;
     	}else{
        	return row.logger+' '+row.message;
     	}
      }
  },{
    className: 'width-1',
    "data": "type",
    "title": "Type",
  },{
    className: 'width-1',
    "data": "date_action",
    "title": "Date and Time",
  }
  ]
  });
  }
</script>