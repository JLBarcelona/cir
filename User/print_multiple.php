<?php
require_once("../config/config.php");
require('../vendor/FPDF/html_table.php');
require_once("../config/auth.php");

$db = new db();
$auth_id = (Auth::check())?  Auth::user('firstname') .' '.Auth::user('lastname') : '';


function get_user($user_type){
	if ($user_type == 1) {
		return 'Administrator';
	}elseif ($user_type == 2) {
		return 'Midwife';
	}elseif ($user_type == 3) {
		return 'Health Worker';
	}
}

$id = $_GET['appid'];

$array = array('child_id' => $id);
$query = "SELECT * from tbl_child where child_id=:child_id";
$res = $db->find($array,$query);


$array2 = array('child_id' => $id);
$query2 = "SELECT a.*,b.* from tbl_appointment a 
INNER JOIN tbl_users b on a.appoint_by=b.user_id 
WHERE child_id=:child_id and a.is_finished is not null";
$res2 = $db->get($array2,$query2);

$child_id = $res->child_id;

$date = date('Y');
$bdate = date('Y', strtotime($res->birthdate));

$mdate = date('m');
$mbdate = date('m', strtotime($res->birthdate));

$m = abs($mdate - $mbdate);
$y = abs($date - $bdate);

$year = ($y < 0)? 0 : $y;
$month = ($year > 0)? '' : ($m > 0)? '.'.$m : '';

$prefix = ($year == 0)? ' Month(s) Old' : ' Year(s) Old';

$age = $year.$month.$prefix;

// $appointment_date = date('F d, Y', strtotime($res->appointment_date));
$appointment_date = '';
$mname = (!empty($res->middlename)) ? ucfirst($res->middlename) : '';
$child_name = ucfirst($res->lastname).' '.ucfirst($res->firstname).', '.$mname;
$addr = 'Purok# '.$res->purok.' '.$res->street.' '.$res->barangay.', '.$res->address;
$status = 'testing';
$data_output = '';
$base_directory = '../webroot/img/signature/'.md5('signature').$id.'.png';





$pdf = new PDF('L','mm','A4');
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Ln(1);
$pdf->SetFont('Times','B',13);
$pdf->Cell(0,0,'Child Ledger',0,1,'C',false);
$pdf->Ln(7);
// $pdf->SetFont('Times','B',13);
// $pdf->Cell(0,0,'March 9, 2021',0,1,'C',false);
// $pdf->Ln(5);



$data_output .='<table border="1">
		<thead>
			<tr>
				<td width="100" bgcolor="#D0D0FF"><b>Child\'S Name</b></td>
				<td width="470">'.$res->lastname.' '.$res->firstname.', '.$res->middlename.'</td>
				<td width="100" bgcolor="#D0D0FF"><b>Birthdate</b></td>
				<td width="100">'.date('F d, Y', strtotime($res->birthdate)).'</td>
				<td width="100" bgcolor="#D0D0FF"><b>Age</b></td>
				<td width="240">'.$age.'</td>
			</tr>
			<tr>
				<td width="100" bgcolor="#D0D0FF"><b>Father\'s Name</b></td>
				<td width="470">'.$res->father_name.'</td>
				<td width="100" bgcolor="#D0D0FF"><b>Mother\'s Name</b></td>
				<td width="440">'.$res->mother_name.'</td>
			</tr>
			<tr>
				<td width="100" bgcolor="#D0D0FF"><b>Address</b></td>
				<td width="1010">'.$addr.'</td>
			</tr>
		</thead>';

$data_output .='<tbody></tbody>';
$data_output .= '</table>';


$data_output .='<table border="0">
			<thead>
				<tr></tr>
			</thead>
		</table>';

$data_output .='<table border="0">
	<thead>
		<tr>
			<td><b>Child Immunization Record</b></td>
		</tr>
	</thead>
</table>';

$data_output .= '<table border="1">
		<thead>
		<tr>
			<td width="90" bgcolor="#D0D0FF"><b>Date</b></td>
			<td width="80" bgcolor="#D0D0FF"><b>Status</b></td>
			<td width="70" bgcolor="#D0D0FF"><b>Weigh(kg)</b></td>
			<td width="70" bgcolor="#D0D0FF"><b>Heigh(cm)</b></td>
			<td width="400" bgcolor="#D0D0FF"><b>Vaccines</b></td>
			<td width="60" bgcolor="#D0D0FF"><b>Doses</b></td>
			<td width="90" bgcolor="#D0D0FF"><b>Date of Injection</b></td>
			<td width="100" bgcolor="#D0D0FF"><b>Site of Injection</b></td>
			<td width="150" bgcolor="#D0D0FF"><b>Remarks</b></td>
		</tr>
	</thead>
	';

foreach ($res2 as $row2 ) {
	$dt = array();
	$dt = [];
	$array_vac = array('appointment_id' => $row2->appointment_id);
	$query_vac = "SELECT a.*,b.* from tbl_appointment_detail 
	a INNER JOIN tbl_vaccine b on a.vac_id=b.vac_id
	where a.appointment_id=:appointment_id and a.is_finished is not null";

	$res_vac = $db->get($array_vac,$query_vac);
	$part = '';
	$is_finished = '';
	$doses = 0;

	$status = '';

	foreach ($res_vac as $key) {
		$dt[] = $key->vaccine_name;
		$part = $key->body_part;
		$is_finished = date('F d, Y', strtotime($key->is_finished));
		$doses++;
	}


	if(empty($row2->is_finished)){
		$status = 'No Appearance';
	}elseif (strtotime(date('Y-m-d', strtotime($row2->is_finished))) > strtotime(date('Y-m-d', strtotime($row2->appointment_date)))) {
		$status = 'No Late';		
	}else{
		$status = 'On Time';		
	}

	$parts = (!empty($part)) ? $part : 'No Data';
	$vaccines = (!empty($dt))? implode(',', $dt) : 'No Data';
	$vac_date = (!empty($is_finished))? $is_finished : 'No Data';
	$remarks = (!empty($row2->description))? $row2->description : 'No Data';
	$data_output .='<table border="1">
		<tbody>
		
			<tr>
				<td width="90">'.date('F d, Y', strtotime($row2->appointment_date)).'</td>
				<td width="80">'.$status.'</td>
				<td width="70">'.$row2->height.'</td>
				<td width="70">'.$row2->weight.'</td>
				<td width="400">'.$vaccines.'</td>
				<td width="60">'.$doses.' of '.$doses.'</td>
				<td width="90">'.$vac_date.'</td>
				<td width="100">'.$parts.'</td>
				<td width="150">'.$remarks.'</td>
			</tr>
			
		</tbody>';

	
}

$data_output .= '</table>';


	// <td width="100" bgcolor="#D0D0FF"><b>Date</b></td>
	// 			<td width="260"></td>
	// 			<td width="100" bgcolor="#D0D0FF"><b>Height</b></td>
	// 			<td width="100"></td>
	// 			<td width="100" bgcolor="#D0D0FF"><b>Weight</b></td>
	// 			<td width="100"></td>
	// <tr>
		// 		<td width="100" bgcolor="#D0D0FF"><b>Date Schedule</b></td>
		// 		<td width="260">'.$row2->appointment_date.'</td>
		// 		<td width="100" bgcolor="#D0D0FF"><b>Height</b></td>
		// 		<td width="100">'.$row2->height.'</td>
		// 		<td width="100" bgcolor="#D0D0FF"><b>Weight</b></td>
		// 		<td width="100">'.$row2->weight.'</td>
		// 	</tr>

// $data_output .='<table border="0">
// 			<thead>
// 				<tr></tr>
// 			</thead>
// 		</table>';


// $data_output .='<table border="0">
// 	<thead>
// 		<tr>
// 			<td><b>Vaccines</b></td>
// 		</tr>
// 	</thead>
// </table>';


// $data_output .='<table border="1">
// 		<thead>
// 			<tr>
// 				<td width="306" bgcolor="#D0D0FF"><b>Vaccine</b></td>
// 				<td width="100" bgcolor="#D0D0FF"><b>Dosses</b></td>
// 				<td width="200" bgcolor="#D0D0FF"><b>Site of Injection</b></td>
// 				<td width="153" bgcolor="#D0D0FF"><b>Date Injected</b></td>

// 			</tr>
// 		</thead><tbody>';
// 		foreach ($res_vac as $row) {
// 			if ($row->appointment_id == $id) {
// 				$data_output .= '<tr>
// 								<td  bgcolor="#D0D0aa" width="306">'.$row->vaccine_name.'</td>
// 								<td  bgcolor="#D0D0aa" width="100">'.$row->dossage.'</td>
// 								<td  bgcolor="#D0D0aa" width="200">'.$row->body_part.'</td>
// 								<td  bgcolor="#D0D0aa" width="153">'.$row->is_finished.'</td>
// 							</tr>';
// 			}else{
// 				$data_output .= '<tr>
// 								<td width="306">'.$row->vaccine_name.'</td>
// 								<td width="100">'.$row->dossage.'</td>
// 								<td width="200">'.$row->body_part.'</td>
// 								<td width="153">'.$row->is_finished.'</td>
// 							</tr>';
// 			}
			
// 		}
// $data_output .='</tbody>';
// $data_output .= '</table>';


// data_output

$pdf->SetFont('Times','',8);
$pdf->WriteHTML($data_output);
$pdf->Ln(10);
$pdf->SetFont('Times','B',10);
// $pdf->Image($base_directory,125,185,100);
$pdf->Cell(0,0,'Prepared by: '.$auth_id,0,1,'R',false);
$pdf->Ln(5);
$pdf->Cell(0,0,get_user(Auth::user('user_type')),0,1,'R',false);
// $pdf->AddPage();
// $pdf->WriteHTML($crop);
$pdf->Output();
?>
