<!DOCTYPE html>
<html>
  <title>Midwife</title>
  <?php include("./Layout/header.php") ?>
  <!-- Header css meta -->
  <style type="text/css">
    .width-less{
      width: 3% !important;
    }
    .width-10{
      width: 10% !important;
    }
  </style>
<body class="sidebar-mini layout-fixed" onload="active_tab('vaccine_tab'); show_vaccines();">
  <div class="wrapper">
  <!-- navbar -->
  <?php include("./Layout/nav.php") ?>
  <!-- Sidebar -->
  <?php include("./Layout/sidebar.php") ?>
   <section class="content-wrapper">
      <div class="container-fluid">
        <div class="row pt-3">
          <div class="col-sm-12">  
            <div class="card">
              <div class="card-header bg-success">
                <span class="h4"><i class="fa fa-syringe"></i> Vaccines</span>
                <button class="btn btn-sm btn-dark float-right" onclick="add_vaccine();"><i class="fa fa-plus"></i></button>
              </div>
              <div class="card-body">
                <table class="table table-bordered dt-responsive nowrap" id="tbl_vaccines" style="width: 100%;"></table>
              </div>
              <div class="card-footer"></div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
</body>
  
   

  <!-- Footer Scripts -->
  <?php include("./Layout/footer.php") ?>
  <?php include("./Vaccine/vaccine_add.php") ?>
</html>
<!-- Javascript Function-->
<script>
  var tbl_vaccines;
  function show_vaccines(){
    if (tbl_vaccines) {
      tbl_vaccines.destroy();
    }
    var url = url_user + '?action=vaccines_list';
    tbl_vaccines = $('#tbl_vaccines').DataTable({
    pageLength: 10,
    responsive: true,
    ajax: url,
    deferRender: true,
    language: {
    "emptyTable": "No data available"
  },
    columns: [{
    className: 'width-10',
    "data": "vaccine_name",
    "title": "Name",
  },{
    className: 'width-1',
    "data": "doses",
    "title": "Doses",
  },{
    className: 'width-1',
    "data": "minimum_age",
    "title": "Minimum Age",
  },{
    className: '',
    "data": "description",
    "title": "Description",
  },{
    className: 'width-1 text-center',
    "data": "vac_id",
    "orderable": false,
    "title": "Options",
      "render": function(data, type, row, meta){
        var param_data = JSON.stringify(row);
        newdata = '';
        newdata += '<button class="btn btn-success btn-sm font-base mt-1" data-info=\' '+param_data.trim()+'\' onclick="edit_vaccines(this)" type="button"><i class="fa fa-edit"></i> Edit</button>';
        newdata += ' <button class="btn btn-danger btn-sm font-base mt-1" data-id=\' '+row.vac_id+'\' onclick="delete_vaccines(this)" type="button"><i class="fa fa-trash"></i> Delete</button>';
        return newdata;
      }
    }
  ]
  });
  }
</script>