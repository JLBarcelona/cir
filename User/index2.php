<!DOCTYPE html>
<html>
<title>Schedule</title>
  <?php include("./Layout/header.php") ?>
  <link rel="stylesheet" href="../webroot/plugins/fullcalendar/main.min.css">
  <link rel="stylesheet" href="../webroot/plugins/fullcalendar-daygrid/main.min.css">
  <link rel="stylesheet" href="../webroot/plugins/fullcalendar-timegrid/main.min.css">
  <link rel="stylesheet" href="../webroot/plugins/fullcalendar-bootstrap/main.min.css">
  <!-- Header css meta -->
<body class="sidebar-mini layout-fixed" onload="active_tab('dashboard_tab'); show_calendar();">
  <div class="wrapper">
  <!-- navbar -->
  <?php include("./Layout/nav.php") ?>
  <!-- Sidebar -->
  <?php include("./Layout/sidebar.php") ?>
   <section class="content-wrapper">
      <div class="container-fluid">
        <div class="row pt-3">
          <div class="col-sm-12">
              <div class="card">
              <div class="card-header bg-success">
                <span class="h4"><i class="fa fa-calendar"></i> Vaccine Schedule</span>
                <button class="btn btn-sm btn-dark float-right" onclick="add_sched('');"><i class="fa fa-plus"></i></button>
              </div>
              <div class="card-body">
                <ul class="nav nav-fill" id="myTab">
                  <li class="nav-item active">
                    <a class="nav-link bg-default border bold" href="schedules.php" role="tab"  aria-selected="true">Calendar</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link bg-primary border bold" id="profile-tab" data-toggle="tab" href="index2.php" role="tab" aria-controls="profile" aria-selected="false">Tabular</a>
                  </li>
              
                </ul>
                <div class="tab-content" id="myTabContent">
                  <div class="tab-pane fade " id="home" role="tabpanel" aria-labelledby="home-tab">
                  </div>
                  <div class="tab-pane fade show active " id="profile" role="tabpanel" aria-labelledby="profile-tab">
                      <br>


                      <div class="card pb-0">
                        <div class="card-header bg-dark h5">
                          Filter
                          <a href="javascript:;" class="close text-white" onclick="$('#filter_content').toggle(); $('.icons').toggleClass('fa-eye fa-eye-slash');"><i class="fa fa-eye icons"></i></a>
                        </div>
                        <div class="card-body hide" id="filter_content">
                          <div class="row">
                            <div class="col-sm-5 col-6 form-group">
                              <input type="date" name="date_from" id="date_from" class="form-control">
                            </div>
                            <div class="col-sm-5 col-6 form-group">
                              <input type="date" name="date_to" id="date_to" class="form-control">
                            </div>
                            <div class="col-sm-2 col-12">
                              <button class="btn btn-sm btn-info" onclick="show_calendar();"><i class="fa fa-search"></i></button>
                              <button class="btn btn-sm btn-primary" onclick="show_calendar(); print_data();"><i class="fa fa-print"></i></button>
                              <button class="btn btn-sm btn-danger" onclick="clear_date();"><i class="fa fa-times"></i></button>
                            </div>
                          </div>
                        </div>
                      </div>

                      <form action="#" id="form_table_sched">
                      <div class="table-responsive">
                       <table class="table table-bordered " id="tbl_children_list" style="width: 100%;">
                          <thead>
                            <tr>
                              <th class="">
                                <input type="checkbox" id="check_all_child" class="actions" onClick="check_uncheck_checkbox(this.checked);">
                              </th>
                              <th>Child Name</th>
                              <th>Month/Age</th>
                              <th>CP#</th>
                              <th>Date Appointment</th>
                              <th>Date Finished</th>
                              <th class="text-center">Status</th>
                              <th>
                                 <!-- <button disabled="" title="Mark all checked as Finish" class="all_action btn btn-success btn-sm font-base mt-1" onclick="finish_all_check();" type="button"><i class="fa fa-check"></i> </button> -->

                                 <button disabled="" title="Remove all checked from schedule" class="all_action btn btn-danger btn-sm font-base mt-1" onclick="delete_all_check();" type="button"><i class="fa fa-times"></i> </button>
                              </th>
                            </tr>
                          </thead>
                        </table>
                      </div>
                    </form>
                  </div>
                </div>
             
              </div>
              <div class="card-footer"></div>
            </div>
            
          </div>
        </div>
      </div>
    </section>
  </div>
</body>



<!-- sorting_disabled -->
  <!-- Footer Scripts -->
  <?php include("./Layout/footer.php") ?>
  <script src="../webroot/plugins/moment/moment.min.js"></script>
  <script src="../webroot/plugins/fullcalendar/main.min.js"></script>
  <script src="../webroot/plugins/fullcalendar-daygrid/main.min.js"></script>
  <script src="../webroot/plugins/fullcalendar-timegrid/main.min.js"></script>
  <script src="../webroot/plugins/fullcalendar-interaction/main.min.js"></script>
  <script src="../webroot/plugins/fullcalendar-bootstrap/main.min.js"></script>
</html>

  
  <?php include("./Schedule/sched_add.php") ?>

<script type="text/javascript">
 var tbl_children_list;
 function show_calendar(){
   let date_from = $("#date_from").val();
  let date_to = $("#date_to").val();
  let url = url_user+'?action=get_schedule';  
  $.ajax({
      type:"GET",
      url:url,
      data:{date_from:date_from, date_to:date_to},
      dataType:'json',
      beforeSend:function(){
      },
      success:function(response){
        console.log(response);
       // if (response.status == true) {
       //    swal("Success", response.message, "success");
       // }else{
       //  console.log(response);
       // }
       show_calendar_data(response.data);
      },
      error: function(error){
        console.log(error);
      }
    });
 }

 function show_calendar_data(data){
   
    if (tbl_children_list) {
      tbl_children_list.destroy();
    }
    tbl_children_list = $('#tbl_children_list').DataTable({
    pageLength: 10,
    responsive: true,
    data: data,
    deferRender: true,
    language: {
    "emptyTable": "No data available"
  },
    columns: [{
    className: 'text-center width-option-1 ',
    "data": "appointment_id",
    "orderable": false,
    "render": function(data, type, row, meta){

      if (row.status == '' || row.status == null) {
         return '<input type="checkbox" class="select_each" name="selected_child[]" id="selected_childs'+row.appointment_id+'" value="'+row.appointment_id+'">';
      }else{
         return '<input type="checkbox" disabled>';
      }

    
    }
  },{
    className: '',
    "data": "fullname",
    "title": 'Child Name',
    "render": function(data, type, row, meta){
     let mname = (row.middlename == '' || row.middlename == null)? '' : ', '+row.middlename;
     return row.fullname+mname;
    }
  },{
    className: '',
    "data": "child_age",
  },{
    className: '',
    "data": "contact_number",
  },{
    className: '',
    "data": "dt_app",
  },{
    className: '',
    "data": "dt_finished",
  },{
    className: 'text-center',
    "data": "description",
     "render": function(data, type, row, meta){
        return row.status_msg;
      }
  },{
    className: 'width-option-1 text-center',
    "data": "appointment_id",
    "orderable": false,
      "render": function(data, type, row, meta){
        var param_data = JSON.stringify(row);
        newdata = '';
        if (row.status == '' || row.status == null) {
           // newdata += ' <button title="Mark as Finish" class="btn btn-success btn-sm font-base mt-1" data-info=\' '+param_data.trim()+'\' onclick="mark_finish_appointment(\''+row.appointment_id+'\')" type="button"><i class="fa fa-check"></i> </button>';
           newdata += '<a target="_blank" href="sched_details.php?r=d210658dbb82bd59bd7ea38ed51eb283&appid='+row.appointment_id+'" title="Show Details" class="btn btn-info btn-sm font-base" ><i class="fa fa-list-alt"></i> </a>';
           newdata += ' <button title="Remove from schedule" class="btn btn-danger btn-sm font-base" data-info=\' '+param_data.trim()+'\' onclick="delete_appointment(\''+row.appointment_id+'\')" type="button"><i class="fa fa-trash"></i> </button>';
        }else{
          // newdata +='<span class="btn btn-success btn-sm">Finished</span>';
          newdata += '<a target="_blank" href="sched_details.php?r=d210658dbb82bd59bd7ea38ed51eb283&appid='+row.appointment_id+'" title="Show Details" class="btn btn-info btn-sm font-base" ><i class="fa fa-list-alt"></i> </a>';
          newdata += ' <a target="_blank" href="print_per_schedule.php?r=d210658dbb82bd59bd7ea38ed51eb283&appid='+row.appointment_id+'" title="Print" class="btn btn-primary btn-sm font-base" ><i class="fa fa-print"></i> </a>';
        }
        
        return newdata;
      }
    }
  ]
  });

  setTimeout(function(){
    $(".actions").parents('th').removeClass("sorting_asc");
    $(".actions").parents('th').addClass("sorting_disabled");
    // sorting_asc 

      $('.select_each').click(function () {
         var check = $('.select_each:input:checkbox:checked').length;
          if (check > 0) {
            $(".all_action").attr('disabled', false);

          }else{
             $(".all_action").attr('disabled', true);

          }
      });
  });


  }

  function clear_date(){
    $("#date_from").val('');
    $("#date_to").val('');
    show_calendar(); 
  }

   function print_data(){
    let selector = $(".print_btn");
    let date_from = $("#date_from").val();
    let date_to = $("#date_to").val();
    if (date_from == '' || date_to == '') {
      swal("Oops!", "Date from and date to is required!", "warning")
    }else{
      window.location = 'print_sched.php?r=d210658dbb82bd59bd7ea38ed51eb283&from='+date_from+'&to='+date_to;
    }
  }
</script>
