<?php if (Auth::check()): ?>

<!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-light-success elevation-4" style="background-color: #cac8c8 !important;">
    <!-- Brand Logo -->
    <a href="./" class="brand-link">
      <img src="../webroot/img/logo.png"
           alt="AdminLTE Logo"
           class="brand-image img-circle elevation-3 bg-white">
           <span class="brand-text font-weight-light"><?php echo Auth::position() ?></span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar" >
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
           <img src="../webroot/img/img.png" data-src="<?php echo Auth::avatar(); ?>" style="background-image: url('<?php echo Auth::avatar(); ?>
          ');" class="img-bg profile_pict img-circle elevation-2 img-show-full pointer" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><?php echo Auth::fullname() ?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="./" class="nav-link" id="dashboard_tab">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
           <li class="nav-item">
            <a href="schedules.php" class="nav-link" id="sched_tab">
              <i class="nav-icon fas fa-calendar"></i>
              <p>
                Schedules
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="children.php" class="nav-link" id="children_tab">
              <i class="nav-icon fas fa-baby"></i>
              <p>
                Children's Profile
              </p>
            </a>
          </li>
          
           <li class="nav-item">
            <a href="decease.php" class="nav-link" id="death_tab">
              <i class="nav-icon fas fa-cross"></i>
              <p>
                Deceased
              </p>
            </a>
          </li>

          <?php if (Auth::user('user_type') == 1 ): ?>

            <li class="nav-item">
              <a href="vaccines.php" class="nav-link" id="vaccine_tab">
                <i class="nav-icon fas fa-syringe"></i>
                <p>
                  Vaccines
                </p>
              </a>
            </li>

            <li class="nav-item has-treeview" id="account_tree">
              <a href="#" class="nav-link" id="account_tab">
                <i class="nav-icon fas fa-users"></i>
                <p>
                  Manage Users
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="midwife.php" class="nav-link" id="midwife_tab">
                    <i class="fas fa-user-nurse nav-icon"></i>
                    <p>Midwife</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="health_worker.php" class="nav-link" id="health_worker_tab">
                    <i class="fas fa-user-md nav-icon"></i>
                    <p>Health Worker</p>
                  </a>
                </li>
              </ul>
            </li>

            <li class="nav-item">
              <a href="archive.php" class="nav-link" id="archive_tab">
                <i class="nav-icon fas fa-archive"></i>
                <p>
                  Archive
                </p>
              </a>
            </li>

             <li class="nav-item">
              <a href="logs.php" class="nav-link" id="logs_tab">
                <i class="nav-icon fas fa-list"></i>
                <p>
                  Logs
                </p>
              </a>
            </li>
          <?php endif ?>

      
          <!-- <li class="nav-header">EXAMPLES</li> -->
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
  <?php else: ?>
<?php endif ?>
