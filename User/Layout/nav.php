<?php if (Auth::check()): ?>
    <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
     <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>
    
    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <!-- <i class="fas fa-user-circle"></i> -->
             <img src="../webroot/img/img.png" style="background-image: url('<?php echo Auth::avatar(); ?>
          ');" class="img-bg profile_pict img-circle elevation-1" width="25" alt="User Image">
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <span class="dropdown-item dropdown-header"><?php echo Auth::fullname(); ?></span>
          <div class="dropdown-divider"></div>
          <a href="account_settings.php" class="dropdown-item">
            <i class="fas fa-shield-alt mr-2"></i> Account Settings
          </a>
          <div class="dropdown-divider"></div>
            <a href="logout_user.php" class="dropdown-item">
            <i class="fas fa-sign-out-alt"></i> Logout
          </a>
        </div>
      </li>
    </ul>
  </nav>
  <?php else: ?>
    <?php //@header('location:../') ?>
<?php endif ?>