/*
Navicat MySQL Data Transfer

Source Server         : Localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : cir

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2021-04-15 13:11:14
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbl_appointment
-- ----------------------------
DROP TABLE IF EXISTS `tbl_appointment`;
CREATE TABLE `tbl_appointment` (
  `appointment_id` int(255) NOT NULL AUTO_INCREMENT,
  `child_id` int(11) DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `milestone` longblob DEFAULT NULL,
  `appointment_date` date DEFAULT NULL,
  `appointment_time` time DEFAULT NULL,
  `age` varchar(255) DEFAULT '',
  `height` double DEFAULT NULL,
  `weight` double DEFAULT NULL,
  `appoint_by` int(11) DEFAULT NULL,
  `is_finished` datetime DEFAULT NULL,
  `signature` longblob DEFAULT NULL,
  PRIMARY KEY (`appointment_id`),
  KEY `child` (`child_id`),
  CONSTRAINT `child` FOREIGN KEY (`child_id`) REFERENCES `tbl_child` (`child_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tbl_appointment
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_appointment_detail
-- ----------------------------
DROP TABLE IF EXISTS `tbl_appointment_detail`;
CREATE TABLE `tbl_appointment_detail` (
  `detail_id` int(11) NOT NULL AUTO_INCREMENT,
  `appointment_id` int(11) DEFAULT NULL,
  `vac_id` int(11) DEFAULT NULL,
  `child_id` int(11) DEFAULT NULL,
  `dossage` varchar(255) DEFAULT NULL,
  `body_part` varchar(255) DEFAULT NULL,
  `is_finished` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`detail_id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tbl_appointment_detail
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_brgy
-- ----------------------------
DROP TABLE IF EXISTS `tbl_brgy`;
CREATE TABLE `tbl_brgy` (
  `brgy_id` int(255) NOT NULL AUTO_INCREMENT,
  `brgy_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`brgy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tbl_brgy
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_child
-- ----------------------------
DROP TABLE IF EXISTS `tbl_child`;
CREATE TABLE `tbl_child` (
  `child_id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) DEFAULT NULL,
  `middlename` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `nickname` varchar(255) DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `barangay` varchar(255) DEFAULT 'Estrella',
  `purok` varchar(255) DEFAULT NULL,
  `street` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT 'San Mateo Isabela',
  `kind_of_birth` varchar(255) DEFAULT NULL,
  `mother_name` varchar(255) DEFAULT NULL,
  `father_name` varchar(255) DEFAULT NULL,
  `contact_number` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `archive_at` datetime DEFAULT NULL,
  `is_death` int(1) DEFAULT 0,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`child_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tbl_child
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_death_record
-- ----------------------------
DROP TABLE IF EXISTS `tbl_death_record`;
CREATE TABLE `tbl_death_record` (
  `death_id` int(11) NOT NULL AUTO_INCREMENT,
  `child_id` int(11) DEFAULT NULL,
  `cause_of_death` longtext DEFAULT NULL,
  `death_date_time` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `signature` longblob DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`death_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tbl_death_record
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_logs
-- ----------------------------
DROP TABLE IF EXISTS `tbl_logs`;
CREATE TABLE `tbl_logs` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `message` longtext DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tbl_logs
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_milestone
-- ----------------------------
DROP TABLE IF EXISTS `tbl_milestone`;
CREATE TABLE `tbl_milestone` (
  `milestone_id` int(11) NOT NULL,
  `child_id` int(11) DEFAULT NULL,
  `milestone` longtext DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`milestone_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tbl_milestone
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_users
-- ----------------------------
DROP TABLE IF EXISTS `tbl_users`;
CREATE TABLE `tbl_users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `avatar` longblob DEFAULT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` longtext DEFAULT NULL,
  `user_type` int(1) DEFAULT 1 COMMENT '1 = admin 2 = midwife 3 = health worker',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `is_active` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `security_question1` longtext DEFAULT NULL,
  `security_answer1` varchar(255) DEFAULT NULL,
  `security_question2` longtext DEFAULT NULL,
  `security_answer2` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tbl_users
-- ----------------------------
INSERT INTO `tbl_users` VALUES ('2', null, 'Angelica', 'Bantat', '1996-02-14', 'Female', 'Echague', 'angelica', '$2y$10$6vqzXlCFA3sZNkUrecwzLu3bXCYW0PBUuGhRFioAyC6FCYDtONPqC', '2', null, '2021-04-09 20:42:42', null, null, '15', 'What is your favorite color?', 'black', 'What is your nickname?', 'ange');
INSERT INTO `tbl_users` VALUES ('3', null, 'Chin Mae', 'Dagio', '1998-02-14', 'Male', 'Echague', 'chinchin', '$2y$10$6vqzXlCFA3sZNkUrecwzLu3bXCYW0PBUuGhRFioAyC6FCYDtONPqC', '3', null, '2021-04-09 20:43:00', null, null, '15', 'What is your favorite color?', 'black', 'What is your nickname?', 'chin');
INSERT INTO `tbl_users` VALUES ('15', null, 'Pedro', 'San', '1998-02-14', 'Male', 'Echague', 'admin', '$2y$10$1WoL4Dxcja2VinkrYKwXH.kw4UPvo8A7jCtSXtCo25HAvnQrlbGQe', '1', null, '2020-12-27 17:34:18', null, null, null, 'What is your favorite color?', 'black', 'What is your nickname?', 'chin');

-- ----------------------------
-- Table structure for tbl_vaccine
-- ----------------------------
DROP TABLE IF EXISTS `tbl_vaccine`;
CREATE TABLE `tbl_vaccine` (
  `vac_id` int(11) NOT NULL AUTO_INCREMENT,
  `vaccine_name` varchar(255) DEFAULT '',
  `doses` int(11) DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `minimum_age` longtext DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`vac_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of tbl_vaccine
-- ----------------------------
INSERT INTO `tbl_vaccine` VALUES ('14', 'Diphtheria-Pertussis-Tetanus Vaccine', '1', 'Right deltoid region of the arm', 'any', '2021-03-25 13:00:37', '2021-04-09 20:31:11', null, '15');
INSERT INTO `tbl_vaccine` VALUES ('15', 'Oral Polio Vaccine', '3', 'Upper outer portion of the thigh, Vastus Lateralis (L-R-L)', '6 weeks old', '2021-03-25 13:01:02', null, null, '15');
INSERT INTO `tbl_vaccine` VALUES ('16', 'Hepatitis B Vaccine', '3', 'Mouth', '6 weeks old', '2021-03-25 13:01:09', null, null, '15');
INSERT INTO `tbl_vaccine` VALUES ('17', 'Measles Vaccine (not MMR)', '3', 'Upper outer portion of the thigh, Vastus Lateralis (R-L-R)', 'At Birth', '2021-03-25 13:01:20', null, null, '15');
INSERT INTO `tbl_vaccine` VALUES ('18', 'Measles Vaccine', '1', 'Upper outer portion of the arms, Right deltoid', '9 months old', '2021-03-25 13:01:47', null, null, '15');
