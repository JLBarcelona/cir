<?php
class db{
	private $con;
	private $server = 'localhost';
	private $username = 'root';
	private $db_password = '';

	public $status;

	public function __construct(){
		try {
			$this->con = new PDO("mysql:host=".$this->server.";dbname=cir", $this->username, $this->db_password,  [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ]);
			$this->status = "Connected successfully";
		} catch (PDOException $e) {
			$this->status = "Connection failed: " . $e->getMessage();
		}
	}

	public function connectionStatus(){
		echo $this->status;
	}

	public function login($table,$data){
		// $username
		$password = '';
		$password_key = '';
		$datas = array();
		$param = array();
		$dataparam = array();

		$i = 0;
		foreach ($data as $key => $value) {
			if ($i == 0) {
				$param[] = $key;
				$dataparam[] = ":".$key;
				$datas[$key] = $value;
			}else{
				$password = $value;
				$password_key = $key;
			}
			$i++;
		}

		
	    $sql = "SELECT * FROM ".$table." WHERE ".implode(",", $param)." = ".implode(",", $dataparam)."";
		
		$result = $this->query($datas, $sql);

		if ($result->rowCount() > 0) {
			$row = $result->fetch();
			if (password_verify($password, $row->$password_key)) {
				// userAuth($row);
				$this->store_auth($row);
				$this->log('Logged In the System', 'Login', $row->user_id);
				return $this->json(['status' => true, 'message' => 'Login success!', 'user' => $row]);
			}else{
				return $this->json(['status' => false, 'message' => 'Incorrect password!']);
			}
		}else{
				return $this->json(['status' => false, 'message' => 'Incorrect password or username!']);
		}
	}

	public function store_auth($data){
		foreach ($data as $key => $value) {
			$_SESSION[$key] = $value;
		}
	}

	public function save($table,$arrs){
		$parameter = array();
		$data = array();
		$dataparam = array();

		foreach ($arrs as $key => $value) {
			$parameter[] = $key;
			$dataparam[] = ":".$key;
			$data[$key] = $value;
		}

		$sql = "INSERT INTO ".$table."(".implode(",", $parameter).") VALUES(".implode(",", $dataparam).")";

		$result = $this->query($data,$sql);
		if ($result) {
			return true;
		}else{
			return $result;
		}
	}

	public function saveWithLastId($table,$arrs){
		$parameter = array();
		$data = array();
		$dataparam = array();

		foreach ($arrs as $key => $value) {
			$parameter[] = $key;
			$dataparam[] = ":".$key;
			$data[$key] = $value;
		}

		$sql = "INSERT INTO ".$table."(".implode(",", $parameter).") VALUES(".implode(",", $dataparam).")";

		$result = $this->query($data,$sql);
		if ($result) {
			return $this->con->lastInsertId();
		}else{
			return $result;
		}
	}


	public function update($table,$arrs){
		$get_parameter = array();
		$parameter = array();
		$data = array();
		$dataparam = array();
		$selector = "";
		$data_update = array();

		foreach ($arrs as $key => $value) {
			$parameter[] = $key."=:".$key;
			$data[$key] = $value;
		}

		for ($i=0; $i < count($parameter); $i++) { 
			if ($i == 0) {
				$selector = $parameter[$i];
			}else{
				$get_parameter[] = $parameter[$i];
			}
		}
		$sql = "UPDATE ".$table." set ".implode(",", $get_parameter)." WHERE ".$selector;
		$result = $this->query($data,$sql);
		if ($result) {
			return true;
		}else{
			return $result;
		}
	}


	public function softDelete($table, $id, $id_name = ''){
		$data = array('deleted_at' => date('Y-m-d H:i:s A'));
		$clause = (!empty($id_name))? $id_name.'='.$id : 'id='.$id; 
		$sql = "UPDATE ".$table." set deleted_at=:deleted_at WHERE ".$clause;
		$result = $this->query($data,$sql);
		if ($result) {
			return true;
		}else{
			return $result;
		}
	}

	public function delete($table,$arrs){
		$get_parameter = array();
		$parameter = array();
		$data = array();
		$dataparam = array();
		$selector = "";
		$data_update = array();

		foreach ($arrs as $key => $value) {
			$parameter[] = $key."=:".$key;
			$data[$key] = $value;
		}

		for ($i=0; $i < count($parameter); $i++) { 
			if ($i == 0) {
				$selector = $parameter[$i];
			}else{
				$get_parameter[] = $parameter[$i];
			}
		}
		$sql = "DELETE from ".$table." WHERE ".$selector;
		$result = $this->query($data,$sql);
		if ($result) {
			return true;
		}else{
			return $result;
		}
	}

	public function counter($table,$field,$parameter){
		$data = array();
		$sql = "SELECT count(".$field.") from ".$table." ".$parameter;
		$prep = $this->con->prepare($sql);
		$prep->execute($data);
		return $prep->fetchColumn();
	}

	public function sum($table,$field,$parameter){
		$data = array();
		$sql = "SELECT sum(".$field.") from ".$table." ".$parameter;
		$prep = $this->con->prepare($sql);
		$prep->execute($data);
		return $prep->fetchColumn();
	}

	public function max($table,$field,$parameter){
		$data = array();
		$sql = "SELECT max(".$field.") from ".$table." ".$parameter;
		$prep = $this->con->prepare($sql);
		$prep->execute($data);
		return $prep->fetchColumn();
	}
	
	public function count($data,$sql){
		$prep = $this->query($data, $sql);
		return $prep->rowCount();
	}

	public function json($data){
		return json_encode($data);
	}

	public function query($data,$sql){
		$prep = $this->con->prepare($sql);
		$prep->execute($data);
		return $prep;
	}

	public function get($data,$sql){
		$par = $this->query($data, $sql);
		return $par->fetchAll();
	}

	public function find($data,$sql){
		$par = $this->query($data, $sql);
		return $par->fetch();
	}

	public function log($message, $type, $auth_id){
		 $data = array("message" => $message,
		 				"type" => $type,
						'created_at' => date('Y-m-d H:i:s A'),
						'user_id' => $auth_id);

 		if ($this->save('tbl_logs', $data)) {
 			return true;
 		}
	}

}