<?php 
function save_signature($signature, $id, $prefix){
	$sig = file_get_contents($signature);
	$base_directory = '../webroot/img/signature/';
	$file_name = md5('signature').$prefix.$id.'.png';
	$filename = $base_directory.''.$file_name;
	file_put_contents($filename, $sig);
}


function get_percentage($total, $value){
	// Just in case it's zero so it won't show error or nan valuel
	$total_ = ($total == 0)? 0 : $total;
	$value_ = ($value == 0)? 0 : $value;

	// compute percentage from the total value
	if ($total_ == 0 && $value_ == 0) {
		return 0;
	}else{
		$total_percent = 100 / $total_;
		$result =  $value_ * $total_percent;
		return round($result, 1);
	}
}


